package com.gtracer.capstone.utils

import com.android.volley.*
import com.android.volley.toolbox.HttpHeaderParser
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import org.json.JSONObject
import java.io.UnsupportedEncodingException
import java.nio.charset.Charset

class GsonRequest<T>(
    url: String, method: Int, private val clazz: Class<T>,
    private val headers: Map<String, String>?,
    private val listener: Response.Listener<T>, errorListener: Response.ErrorListener
) : Request<T>(method, url, errorListener) {

    private val gson = Gson()
    private var body: JSONObject? = null

    constructor(
        url: String, method: Int, body: JSONObject?, clazz: Class<T>, headers: Map<String, String>?,
        listener: Response.Listener<T>, errorListener: Response.ErrorListener
    ) : this(url, method, clazz, headers, listener, errorListener) {
        this.body = body
    }

    override fun getHeaders(): MutableMap<String, String> = headers as MutableMap<String, String>

    @Throws(AuthFailureError::class)
    override fun getBody(): ByteArray? {
        try {
            return this.body?.toString()?.toByteArray(charset(paramsEncoding))
        } catch (e: UnsupportedEncodingException) {
            e.printStackTrace()
        }
        return null
    }

    override fun deliverResponse(response: T) = listener.onResponse(response)

    override fun parseNetworkResponse(response: NetworkResponse?): Response<T> {
        return try {
            val json = String(
                response?.data ?: ByteArray(0),
                Charset.forName(HttpHeaderParser.parseCharset(response?.headers))
            )

            Response.success(
                gson.fromJson(json, clazz), HttpHeaderParser.parseCacheHeaders(response)
            )
        } catch (e: UnsupportedEncodingException) {
            Response.error(ParseError(e))
        } catch (e: JsonSyntaxException) {
            Response.error(ParseError(e))
        }
    }
}