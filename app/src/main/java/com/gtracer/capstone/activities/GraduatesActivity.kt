package com.gtracer.capstone.activities

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.gtracer.capstone.adapters.ViewPagerAdapter
import com.gtracer.capstone.databinding.ActivityGraduatesBinding
import com.gtracer.capstone.fragments.PendingFragment
import com.gtracer.capstone.fragments.SubmittedFragment

class GraduatesActivity : AppCompatActivity() {

    private lateinit var mBinding: ActivityGraduatesBinding

    companion object {
        private val TAG = GraduatesActivity::class.java.simpleName
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityGraduatesBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        setSupportActionBar(mBinding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(true)
        val adapter = ViewPagerAdapter(this, supportFragmentManager)
        adapter.addFragment(PendingFragment())
        adapter.addFragment(SubmittedFragment())
        mBinding.viewPager.adapter = adapter
        mBinding.tabs.setupWithViewPager(mBinding.viewPager)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home)
            finish()

        return super.onOptionsItemSelected(item)
    }
}
