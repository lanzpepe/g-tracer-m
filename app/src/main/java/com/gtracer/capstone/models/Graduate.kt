package com.gtracer.capstone.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class Graduate(
    @SerializedName("graduate_id") var graduateId: String? = null,
    @SerializedName("image_uri") var imageUri: String? = null,
    @SerializedName("responses_count") var responsesCount: Int = 0,
    @SerializedName("confirmed") var confirmedList: ArrayList<Confirmed>? = null,
    var person: Person? = null, var respondent: Respondent? = null,
    var academic: Education? = null, var contacts: ArrayList<Contact>? = null,
    var users: ArrayList<User>? = null
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readString(), parcel.readString(), parcel.readInt(),
        parcel.createTypedArrayList(Confirmed.CREATOR),
        parcel.readParcelable(Person::class.java.classLoader),
        parcel.readParcelable(Respondent::class.java.classLoader),
        parcel.readParcelable(Education::class.java.classLoader),
        parcel.createTypedArrayList(Contact.CREATOR),
        parcel.createTypedArrayList(User.CREATOR)
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(graduateId)
        parcel.writeString(imageUri)
        parcel.writeInt(responsesCount)
        parcel.writeTypedList(confirmedList)
        parcel.writeParcelable(person, flags)
        parcel.writeParcelable(respondent, flags)
        parcel.writeParcelable(academic, flags)
        parcel.writeTypedList(contacts)
        parcel.writeTypedList(users)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Graduate> {
        override fun createFromParcel(parcel: Parcel): Graduate {
            return Graduate(parcel)
        }

        override fun newArray(size: Int): Array<Graduate?> {
            return arrayOfNulls(size)
        }
    }
}