package com.gtracer.capstone.fragments

import android.annotation.SuppressLint
import android.content.Intent
import android.location.Geocoder
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.ScrollView
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.android.volley.Request
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.gtracer.capstone.R
import com.gtracer.capstone.activities.RegisterActivity
import com.gtracer.capstone.databinding.FragmentBasicInfoBinding
import com.gtracer.capstone.models.Contact
import com.gtracer.capstone.models.Person
import com.gtracer.capstone.models.User
import com.gtracer.capstone.objects.Companions
import com.gtracer.capstone.objects.Constant
import com.gtracer.capstone.singletons.AppInstance
import com.gtracer.capstone.singletons.AppVolley.Callback
import org.json.JSONObject
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class BasicInfoFragment : Fragment(), View.OnFocusChangeListener, View.OnTouchListener, Callback {

    private var _binding: FragmentBasicInfoBinding? = null
    private var mContact: Contact? = null
    private var mPerson: Person? = null
    private var mUser: User? = null
    private var mGenderAdapter: ArrayAdapter<String>? = null
    private val mBinding get() = _binding

    companion object {
        private val TAG = BasicInfoFragment::class.java.simpleName
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentBasicInfoBinding.inflate(inflater, container, false)
        return mBinding?.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initInstances(); getGenders(); setListeners()
        mBinding?.nav?.btnPrev?.visibility = View.GONE
        mBinding?.nav?.btnNext?.setOnClickListener {
            saveBasicInfo()
        }
    }

    private fun initInstances() {
        mContact = AppInstance.getContact()
        mPerson = AppInstance.getPerson()
        mUser = AppInstance.getUser()
    }

    private fun getGenders() {
        HashMap<String, String>().let { header ->
            header["Accept"] = resources.getString(R.string.text_accept)
            Companions.makeCacheRequest(Request.Method.GET, Companions.genders(), header, this)
        }
    }

    override fun onSuccess(result: JSONObject?) {
        val jsonArray = result?.getJSONArray("genders")
        val genders = ArrayList<String>()

        for (i in 0 until jsonArray!!.length()) {
            genders.add(jsonArray.getJSONObject(i).getString("name"))
        }

        mGenderAdapter = ArrayAdapter(context!!, R.layout.item_spinner, genders)
        mGenderAdapter?.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        mBinding?.spnRegGender?.adapter = mGenderAdapter
        setValues(mGenderAdapter)
    }

    override fun onFailure(result: String?) {
        Companions.debugLog(TAG, result)
    }

    private fun setValues(adapter: ArrayAdapter<String>?) {
        mBinding?.atvRegFirstName?.setText(mPerson?.firstName)
        mBinding?.atvRegMidName?.setText(mPerson?.middleName)
        mBinding?.atvRegLastName?.setText(mPerson?.lastName)
        mBinding?.tvRegBirthDate?.text = mPerson?.birthDate
        if (mPerson?.gender != null)
            mBinding?.spnRegGender?.setSelection(adapter!!.getPosition(mPerson?.gender))
        mBinding?.atvRegAddressLine?.setText(mContact?.address)
        mBinding?.atvRegCity?.setText(mContact?.city)
        mBinding?.atvRegProvince?.setText(mContact?.province)
        mBinding?.atvRegContact?.setText(mContact?.contactNumber)
        mBinding?.atvRegEmail?.setText(mContact?.email)
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun setListeners() {
        mBinding?.atvRegFirstName?.onFocusChangeListener = this
        mBinding?.atvRegMidName?.onFocusChangeListener = this
        mBinding?.atvRegLastName?.onFocusChangeListener = this
        mBinding?.tvRegBirthDate?.onFocusChangeListener = this
        mBinding?.atvRegAddressLine?.onFocusChangeListener = this
        mBinding?.atvRegCity?.onFocusChangeListener = this
        mBinding?.atvRegProvince?.onFocusChangeListener = this
        mBinding?.atvRegContact?.onFocusChangeListener = this
        mBinding?.atvRegEmail?.onFocusChangeListener = this
        mBinding?.layoutBasicInfo?.setOnTouchListener(this)
        mBinding?.tvRegBirthDate?.setOnTouchListener(this)
        mBinding?.atvRegAddressLine?.setOnTouchListener(this)
    }

    override fun onFocusChange(v: View?, hasFocus: Boolean) {
        when (v!!.id) {
            R.id.atvRegFirstName, R.id.atvRegMidName, R.id.atvRegLastName, R.id.tvRegBirthDate,
            R.id.atvRegAddressLine, R.id.atvRegCity, R.id.atvRegProvince, R.id.atvRegContact,
            R.id.atvRegEmail -> Companions.setBackgroundError(v, false)
        }
    }

    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
        v?.performClick()

        if (event?.action == MotionEvent.ACTION_DOWN) {
            when (v!!.id) {
                R.id.layoutBasicInfo -> Companions.hideKeyboard(activity!!, v)
                R.id.tvRegBirthDate ->
                    if (event.rawX >= v.right.minus((v as TextView).totalPaddingEnd))
                        Companions.date(context, mBinding?.tvRegBirthDate)
                R.id.atvRegAddressLine -> {
                    if (event.rawX >= v.right.minus((v as AutoCompleteTextView).totalPaddingEnd)) {
                        val intent = Autocomplete.IntentBuilder(
                            AutocompleteActivityMode.OVERLAY,
                            arrayListOf(Place.Field.NAME, Place.Field.LAT_LNG)
                        ).setCountry("PH").build(context!!)
                        startActivityForResult(intent, Constant.AUTOCOMPLETE_REQUEST_CODE)
                    }
                }
            }
        } else {
            v?.requestFocus()
        }

        return false
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == Constant.AUTOCOMPLETE_REQUEST_CODE) {
            when (resultCode) {
                AutocompleteActivity.RESULT_OK -> {
                    val place = Autocomplete.getPlaceFromIntent(data!!)
                    val lat = place.latLng!!.latitude
                    val lng = place.latLng!!.longitude
                    val geocoder = Geocoder(context, Locale.getDefault())

                    try {
                        val addresses = geocoder.getFromLocation(lat, lng, 1)
                        mBinding?.atvRegAddressLine?.setText(place.name)
                        mBinding?.atvRegCity?.setText(addresses[0].locality)
                        mBinding?.atvRegProvince?.setText(addresses[0].subAdminArea)
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                }
                AutocompleteActivity.RESULT_ERROR -> {
                    val status = Autocomplete.getStatusFromIntent(data!!)
                    Companions.toast(status.statusMessage)
                }
            }
        }

        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun saveBasicInfo() {
        val firstName = mBinding?.atvRegFirstName?.text.toString().trim()
        val middleName = mBinding?.atvRegMidName?.text.toString().trim()
        val lastName = mBinding?.atvRegLastName?.text.toString().trim()
        val birthDate = mBinding?.tvRegBirthDate?.text.toString().trim()
        val gender = mBinding?.spnRegGender?.selectedItem.toString()
        val addressLine = mBinding?.atvRegAddressLine?.text.toString().trim()
        val city = mBinding?.atvRegCity?.text.toString().trim()
        val province = mBinding?.atvRegProvince?.text.toString().trim()
        val contactNumber = mBinding?.atvRegContact?.text.toString().trim()
        val email = mBinding?.atvRegEmail?.text.toString().trim()
        val imageUri = mUser?.imageUri

        if (!isError(mBinding?.atvRegFirstName, firstName, "First name") &&
            !isError(mBinding?.atvRegMidName, middleName, "Middle name") &&
            !isError(mBinding?.atvRegLastName, lastName, "Last name") &&
            !isError(mBinding?.tvRegBirthDate, birthDate, "Birth date") &&
            !isError(mBinding?.atvRegAddressLine, addressLine, "Address line") &&
            !isError(mBinding?.atvRegCity, city, "City") &&
            !isError(mBinding?.atvRegProvince, province, "Province") &&
            !isError(mBinding?.atvRegContact, contactNumber, "Contact number") &&
            !isError(mBinding?.atvRegEmail, email, "Email address")
        ) {
            mPerson?.lastName = lastName; mPerson?.firstName = firstName
            mPerson?.middleName = middleName; mPerson?.birthDate = birthDate
            mPerson?.gender = gender; mUser?.imageUri = imageUri
            mContact?.address = addressLine; mContact?.city = city; mContact?.province = province
            mContact?.contactNumber = contactNumber; mContact?.email = email

            replaceFragment(EducationFragment())
            (activity as RegisterActivity).getScrollView().fullScroll(ScrollView.FOCUS_UP)
        }
    }

    private fun replaceFragment(otherFragment: Fragment) =
        Companions.replaceFragment(activity!!, otherFragment)

    private fun isError(view: View?, input: String?, tag: String?) =
        Companions.isError(view, input, tag)
}
