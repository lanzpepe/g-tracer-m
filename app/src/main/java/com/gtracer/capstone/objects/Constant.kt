package com.gtracer.capstone.objects

object Constant {
    const val AUTOCOMPLETE_REQUEST_CODE = 1000
    const val RESPONSE_REQUEST_CODE = 2000
    const val TIMEOUT_MS = 5000
    const val TUTORIAL = "tutorial"
    const val ACCESS_TOKEN = "access_token"
    const val EXPIRES_IN = "expires_in"
    const val GRADUATE = "graduate"
    const val GRADUATES = "graduates"
    const val REFRESH_TOKEN = "refresh_token"
    const val PROVIDER_ID = "provider_id"
    const val PROVIDER_NAME = "provider_name"
    const val PROVIDER_URL = "provider_url"
    const val USER = "user"
    const val USER_ID = "user_id"
    const val FIRST_NAME = "first_name"
    const val LAST_NAME = "last_name"
    const val EMAIL_ADDRESS = "email_address"
    const val IMAGE_URI = "image_uri"
}