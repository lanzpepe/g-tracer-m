package com.gtracer.capstone.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class Twitter(
    @SerializedName("id_str") var id: String? = null,
    @SerializedName("screen_name") var username: String? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString()
    )
    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(username)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Twitter> {
        override fun createFromParcel(parcel: Parcel): Twitter {
            return Twitter(parcel)
        }

        override fun newArray(size: Int): Array<Twitter?> {
            return arrayOfNulls(size)
        }
    }
}