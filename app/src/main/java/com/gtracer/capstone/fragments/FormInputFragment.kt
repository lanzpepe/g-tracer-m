package com.gtracer.capstone.fragments

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentActivity
import com.android.volley.Request
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.gtracer.capstone.R
import com.gtracer.capstone.activities.ProfileActivity
import com.gtracer.capstone.activities.ResponseActivity
import com.gtracer.capstone.databinding.DialogInputBinding
import com.gtracer.capstone.models.Graduate
import com.gtracer.capstone.models.User
import com.gtracer.capstone.objects.Companions
import com.gtracer.capstone.objects.Constant
import com.gtracer.capstone.singletons.AppVolley.Callback
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class FormInputFragment : DialogFragment(), View.OnTouchListener {

    private var mGraduate: Graduate? = null
    private var mUser: User? = null
    private var mCompanyAdapter: ArrayAdapter<String>? = null
    private var mJobAdapter: ArrayAdapter<String>? = null
    private var mRemarksAdapter: ArrayAdapter<String>? = null
    private var mSimpleDateFormat: SimpleDateFormat? = null
    private lateinit var mView: DialogInputBinding

    companion object {
        private val TAG = FormInputFragment::class.java.simpleName
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog: AlertDialog

        mRemarksAdapter = ArrayAdapter(
            context!!, R.layout.item_spinner, resources.getStringArray(R.array.remarks)
        )
        mRemarksAdapter?.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        mSimpleDateFormat = SimpleDateFormat("MMMM dd, yyyy", Locale.US)
        mView = DialogInputBinding.inflate(LayoutInflater.from(context!!))

        if (arguments!!.containsKey(Constant.USER)) {
            mUser = arguments?.getParcelable(Constant.USER)
            mView.tvUpdateTitle.text = context!!.resources.getString(R.string.text_update_info)
            mView.tvUpdateHeader.text = mUser?.person?.toString()
            mView.layoutDateEmployed.visibility = View.VISIBLE
            mView.layoutFooter.visibility = View.GONE
            getCompanies(); getJobs()
        }

        if (arguments!!.containsKey(Constant.GRADUATE)) {
            mGraduate = arguments?.getParcelable(Constant.GRADUATE)
            mView.tvUpdateTitle.text = context?.resources?.getString(R.string.text_survey_title)
            mView.tvUpdateHeader.text = resources.getString(
                R.string.text_survey_header, mGraduate?.person?.toString()
            )
            mView.tvUpdateRemarks.text = resources.getString(
                R.string.text_remarks, mGraduate?.person?.firstName
            )
            mView.tvUpdateFooter.text = resources.getString(R.string.text_survey_footer)
            mView.layoutRemarks.visibility = View.VISIBLE
            mView.spnRemarks.adapter = mRemarksAdapter
            getCompanies(); getJobs()
        }

        val text = if (arguments!!.containsKey(Constant.GRADUATE)) "Submit" else "Update"
        mView.atvUpdateCoAddress.setOnTouchListener(this)
        mView.atvUpdateDateEmployed.setOnTouchListener(this)
        mView.root.setOnTouchListener(this)
        AlertDialog.Builder(context!!).let { builder ->
            builder.setView(mView.root).setCancelable(false).setPositiveButton(text, null)
                .setNegativeButton("Cancel", null)
            dialog = builder.create()
        }
        dialog.show(); dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener {
            if (tag.equals(ProfileActivity.TAG, true)) {
                updateProfile(dialog)
            } else {
                postResponse(activity)
            }
        }
        dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener {
            dialog.dismiss()
        }

        return dialog
    }

    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
        v?.performClick()

        if (event?.action == MotionEvent.ACTION_DOWN) {
            when (v!!.id) {
                R.id.atvUpdateCoAddress -> {
                    if (event.x >= v.right.minus((v as AutoCompleteTextView).totalPaddingEnd)) {
                        val intent = Autocomplete.IntentBuilder(
                            AutocompleteActivityMode.OVERLAY,
                            /* arrayListOf(Place.Field.NAME, Place.Field.LAT_LNG) */
                            arrayListOf(Place.Field.ADDRESS)
                        ).build(context!!)
                        startActivityForResult(intent, Constant.AUTOCOMPLETE_REQUEST_CODE)
                    } else v.requestFocus()
                }
                R.id.atvUpdateDateEmployed -> {
                    if (event.x >= v.right.minus((v as AutoCompleteTextView).totalPaddingEnd)) {
                        Companions.date(context, v)
                    } else v.requestFocus()
                }
                else -> Companions.hideKeyboard(activity!!, mView.root)
            }

            return true
        }

        return false
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == Constant.AUTOCOMPLETE_REQUEST_CODE) {
            when (resultCode) {
                AutocompleteActivity.RESULT_OK -> {
                    val place = Autocomplete.getPlaceFromIntent(data!!)
                    /* val coordinate = arrayOf(place.latLng!!.latitude, place.latLng!!.longitude)
                    val geocoder = Geocoder(context!!, Locale.getDefault())

                    try {
                        val addresses = geocoder.getFromLocation(coordinate[0], coordinate[1], 1)
                        mView.atvUpdateCoAddress.setText(
                            resources.getString(
                                R.string.text_complete_address,
                                place.name, addresses[0].locality, addresses[0].subAdminArea
                            )
                        )
                    } catch (e: IOException) {
                        e.printStackTrace()
                    } */
                    mView.atvUpdateCoAddress.setText(place.address)
                }
                AutocompleteActivity.RESULT_ERROR -> {
                    val status = Autocomplete.getStatusFromIntent(data!!)
                    Companions.toast(status.statusMessage)
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun getCompanies() {
        HashMap<String, String>().let { header ->
            header["Accept"] = resources.getString(R.string.text_accept)
            Companions.makeJsonRequest(Request.Method.GET, Companions.companies(), null, header,
                object : Callback {
                    override fun onSuccess(result: JSONObject?) {
                        val jsonArray = result?.getJSONArray("companies")
                        val companyList = ArrayList<String>()

                        for (i in 0 until jsonArray!!.length()) {
                            companyList.add(jsonArray.getJSONObject(i).getString("name"))
                        }
                        mCompanyAdapter = ArrayAdapter(
                            context!!, android.R.layout.simple_spinner_dropdown_item, companyList
                        )
                        mView.atvUpdateCoName.threshold = 1
                        mView.atvUpdateCoName.setAdapter(mCompanyAdapter)
                    }

                    override fun onFailure(result: String?) {
                        Companions.debugLog(TAG, result)
                    }
                }
            )
        }
    }

    private fun getJobs() {
        HashMap<String, String>().let { header ->
            header["Accept"] = this.resources.getString(R.string.text_accept)
            Companions.makeJsonRequest(Request.Method.GET, Companions.jobs(), null, header,
                object : Callback {
                    override fun onSuccess(result: JSONObject?) {
                        val jsonArray = result?.getJSONArray("employments")
                        val jobList = ArrayList<String>()

                        for (i in 0 until jsonArray!!.length()) {
                            jobList.add(jsonArray.getJSONObject(i).getString("job_position"))
                        }
                        mJobAdapter = ArrayAdapter(
                            context!!, android.R.layout.simple_spinner_dropdown_item, jobList
                        )
                        mView.atvUpdatePosition.threshold = 1
                        mView.atvUpdatePosition.setAdapter(mJobAdapter)
                    }

                    override fun onFailure(result: String?) {
                        Companions.debugLog(TAG, result)
                    }
                }
            )
        }
    }

    private fun updateProfile(dialog: AlertDialog) {
        val companyName = mView.atvUpdateCoName.text.toString().trim()
        val companyAddress = mView.atvUpdateCoAddress.text.toString().trim()
        val jobPosition = mView.atvUpdatePosition.text.toString().trim()
        val dateEmployed = mView.atvUpdateDateEmployed.text.toString().trim()

        if (!isError(mView.atvUpdateCoName, companyName, "Company name") &&
            !isError(mView.atvUpdateCoAddress, companyAddress, "Company address") &&
            !isError(mView.atvUpdatePosition, jobPosition, "Job position") &&
            !isError(mView.atvUpdateDateEmployed, dateEmployed, "Date employed")
        ) {
            HashMap<String, String>().let { body ->
                body["companyName"] = companyName
                body["companyAddress"] = companyAddress
                body["jobPosition"] = jobPosition
                body["dateEmployed"] = mSimpleDateFormat
                    ?.format(Date(Companions.getTime(dateEmployed)))!!
                HashMap<String, String>().let { header ->
                    header["Content-Type"] = resources.getString(R.string.text_accept)
                    header["Authorization"] = resources.getString(
                        R.string.text_authorization,
                        Companions.getAccessToken()
                    )
                    storeProfile(body, header)
                }
            }
            dialog.dismiss()
        }
    }

    private fun storeProfile(vararg maps: Map<String, String>) {
        Companions.makeJsonRequest(Request.Method.POST, Companions.update(),
            JSONObject(maps[0]), maps[1], object : Callback {
                override fun onSuccess(result: JSONObject?) {
                    (activity as OnDialogFragmentListener).onDismiss(result?.getString("message"))
                }

                override fun onFailure(result: String?) {
                    (activity as OnDialogFragmentListener).onDismiss(result)
                }
            }
        )
    }

    private fun postResponse(fragment: FragmentActivity?) {
        val companyName = mView.atvUpdateCoName.text.toString().trim()
        val companyAddress = mView.atvUpdateCoAddress.text.toString().trim()
        val jobPosition = mView.atvUpdatePosition.text.toString().trim()
        val remarks = mView.spnRemarks.selectedItem.toString().trim()

        if (!isError(mView.atvUpdateCoName, companyName, "Company name") &&
            !isError(mView.atvUpdateCoAddress, companyAddress, "Company address") &&
            !isError(mView.atvUpdatePosition, jobPosition, "Job position")
        ) {
            HashMap<String, String>().let { body ->
                body["graduateId"] = mGraduate?.graduateId!!
                body["companyName"] = companyName
                body["companyAddress"] = companyAddress
                body["jobPosition"] = jobPosition
                body["remarks"] = remarks
                HashMap<String, String>().let { header ->
                    header["Content-Type"] = resources.getString(R.string.text_accept)
                    header["Authorization"] = resources.getString(
                        R.string.text_authorization,
                        Companions.getAccessToken()
                    )
                    sendResponse(fragment, header, body)
                }
            }

            dismiss()
        }
    }

    private fun sendResponse(fragment: FragmentActivity?, vararg maps: Map<String, String>) {
        Companions.makeJsonRequest(Request.Method.POST, Companions.response(),
            JSONObject(maps[1]), maps[0], object : Callback {
                override fun onSuccess(result: JSONObject?) {
                    if (fragment is ResponseActivity) {
                        (fragment as OnDialogFragmentListener).onDismiss(result?.getString("message"))
                    } else {
                        targetFragment?.onActivityResult(
                            targetRequestCode, Activity.RESULT_OK,
                            Intent().putExtra("message", result?.getString("message"))
                        )
                    }
                }

                override fun onFailure(result: String?) {
                    if (fragment is ResponseActivity) {
                        (fragment as OnDialogFragmentListener).onDismiss(result)
                    } else {
                        targetFragment?.onActivityResult(
                            targetRequestCode, Activity.RESULT_OK,
                            Intent().putExtra("message", result)
                        )
                    }
                }
            }
        )
    }

    private fun isError(view: View?, input: CharSequence?, tag: String?) =
        Companions.isError(view, input, tag)

    interface OnDialogFragmentListener {
        fun onDismiss(message: String?)
    }
}