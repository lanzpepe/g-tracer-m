package com.gtracer.capstone.models

import android.os.Parcel
import android.os.Parcelable

data class School(var id: String? = null, var name: String? = null, var logo: String? = null,
                  var departments: ArrayList<Department>? = null,
                  var courses: ArrayList<Course>? = null) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.createTypedArrayList(Department.CREATOR),
        parcel.createTypedArrayList(Course.CREATOR)
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(name)
        parcel.writeString(logo)
        parcel.writeTypedList(departments)
        parcel.writeTypedList(courses)
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun toString(): String = name!!

    companion object CREATOR : Parcelable.Creator<School> {
        override fun createFromParcel(parcel: Parcel): School {
            return School(parcel)
        }

        override fun newArray(size: Int): Array<School?> {
            return arrayOfNulls(size)
        }
    }
}