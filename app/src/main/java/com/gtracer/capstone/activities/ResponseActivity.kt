package com.gtracer.capstone.activities

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.android.volley.Request
import com.gtracer.capstone.R
import com.gtracer.capstone.fragments.FormInputFragment
import com.gtracer.capstone.models.Graduate
import com.gtracer.capstone.objects.Companions
import com.gtracer.capstone.objects.Constant
import com.gtracer.capstone.singletons.AppVolley.Callback
import com.gtracer.capstone.threads.ImageTask
import com.google.gson.Gson
import com.gtracer.capstone.databinding.ActivityResponseBinding
import org.json.JSONObject

class ResponseActivity : AppCompatActivity(), View.OnClickListener, Callback,
    FormInputFragment.OnDialogFragmentListener {

    private var mGraduate: Graduate? = null
    private var mUserId: String? = null
    private lateinit var mBinding: ActivityResponseBinding

    companion object {
        private val TAG = ResponseActivity::class.java.simpleName
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityResponseBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        setSupportActionBar(mBinding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(true)
        mGraduate = intent?.getParcelableExtra(Constant.GRADUATE)
        mUserId = intent?.getStringExtra(Constant.USER_ID)
        mBinding.profile.tvProfileAddress.visibility = View.GONE
        mBinding.profile.tvProfileContact.visibility = View.GONE
        mBinding.profile.tvProfileEmail.visibility = View.GONE
        mBinding.tvEmploymentStatus.text =
            resources.getString(R.string.text_employment_status, "Not Confirmed")
        setValues(mGraduate?.graduateId)
        mBinding.btnSubmit.setOnClickListener(this)
    }

    private fun setValues(id: String?) {
        HashMap<String, String>().let { header ->
            header["Accept"] = resources.getString(R.string.text_accept)
            header["Authorization"] =
                resources.getString(R.string.text_authorization, Companions.getAccessToken())
            Companions.makeJsonRequest(
                Request.Method.GET, Companions.graduate(id), null, header, this
            )
        }
    }

    override fun onSuccess(result: JSONObject?) {
        val json = result?.getJSONObject(Constant.GRADUATE)
        val graduate = Gson().fromJson(json?.toString(), Graduate::class.java)
        val uri = if (graduate.person?.gender == "Male")
            ContextCompat.getDrawable(this, R.drawable.default_avatar_m)
        else
            ContextCompat.getDrawable(this, R.drawable.default_avatar_f)
        val fullName = graduate.person?.toString()
        val url = Companions.imageUri(graduate.imageUri).replace("+", "%20")
        val status = if (graduate.confirmedList!!.isEmpty()) "Open" else "Closed"
        ImageTask(mBinding.profile.ivProfileImage, url, uri!!).execute(url)
        mBinding.profile.tvProfileName.text = fullName
        mBinding.education.tvDegree.text = graduate.academic?.course?.title
        if (graduate.academic?.course?.major!!.equals("None", true))
            mBinding.education.tvMajor.visibility = View.GONE
        else
            mBinding.education.tvMajor.text =
                resources.getString(R.string.text_major_profile, graduate.academic?.course?.major)
        mBinding.education.tvSchool.text = graduate.academic?.school
        mBinding.education.tvDateGraduated.text = resources?.getString(
            R.string.text_date_graduated, graduate.academic?.month,
            graduate.academic?.day, graduate.academic?.year
        )
        mBinding.tvResponseCount.text =
            resources.getString(R.string.text_response_count, graduate.responsesCount)
        mBinding.tvEmploymentStatus.text = resources.getString(R.string.text_employment_status, status)

        for (i in 0 until graduate.users!!.size) {
            val respondent = graduate.users!![i].respondent
            if (respondent?.userId == mUserId && respondent?.responseId != null) {
                mBinding.btnSubmit.visibility = View.GONE
                mBinding.btnSubmitted.visibility = View.VISIBLE
                break
            } else {
                mBinding.btnSubmit.visibility = View.VISIBLE
                mBinding.btnSubmitted.visibility = View.GONE
            }
        }
    }

    override fun onFailure(result: String?) {
        Companions.debugLog(TAG, result)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.btnSubmit -> {
                Bundle().let { data ->
                    data.putString(Constant.USER_ID, mUserId)
                    data.putParcelable(Constant.GRADUATE, mGraduate)
                    FormInputFragment().let { fragment ->
                        fragment.arguments = data
                        fragment.show(supportFragmentManager, TAG)
                    }
                }
            }
        }
    }

    override fun onDismiss(message: String?) {
        val dialog: AlertDialog

        setValues(mGraduate?.graduateId)

        AlertDialog.Builder(this).let { builder ->
            builder.setMessage(message).setCancelable(false)
                .setPositiveButton("OK") { dialog, _ ->
                dialog.dismiss()
            }
            dialog = builder.create()
            dialog.show()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home)
            finish()

        return super.onOptionsItemSelected(item)
    }
}