package com.gtracer.capstone.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class Person(
    @SerializedName("person_id") var personId: String? = null,
    @SerializedName("last_name") var lastName: String? = null,
    @SerializedName("first_name") var firstName: String? = null,
    @SerializedName("middle_name") var middleName: String? = null,
    var gender: String? = null, @SerializedName("birth_date") var birthDate: String? = null
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(personId)
        parcel.writeString(lastName)
        parcel.writeString(firstName)
        parcel.writeString(middleName)
        parcel.writeString(gender)
        parcel.writeString(birthDate)
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun toString(): String {
        return "$firstName $middleName $lastName"
    }

    companion object CREATOR : Parcelable.Creator<Person> {
        override fun createFromParcel(parcel: Parcel): Person {
            return Person(parcel)
        }

        override fun newArray(size: Int): Array<Person?> {
            return arrayOfNulls(size)
        }
    }
}