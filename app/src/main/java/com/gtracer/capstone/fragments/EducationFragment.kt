package com.gtracer.capstone.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ScrollView
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import com.android.volley.Request
import com.google.gson.Gson
import com.gtracer.capstone.R
import com.gtracer.capstone.activities.RegisterActivity
import com.gtracer.capstone.databinding.FragmentEducationBinding
import com.gtracer.capstone.models.*
import com.gtracer.capstone.objects.Companions
import com.gtracer.capstone.singletons.AppInstance
import com.gtracer.capstone.singletons.AppVolley.Callback
import org.json.JSONObject

class EducationFragment : Fragment(), View.OnClickListener, View.OnFocusChangeListener,
    View.OnTouchListener {

    private var _binding: FragmentEducationBinding? = null
    private var mContact: Contact? = null
    private var mCourse: Course? = null
    private var mEducation: Education? = null
    private var mPerson: Person? = null
    private var mCourseAdapter: ArrayAdapter<Course>? = null
    private var mMajorAdapter: ArrayAdapter<String>? = null
    private var mSchoolAdapter: ArrayAdapter<School>? = null
    private var mSchoolYearAdapter: ArrayAdapter<String>? = null
    private var mBatchAdapter: ArrayAdapter<String>? = null
    private var mResourceId: Int = 0
    private lateinit var mDialog: AlertDialog
    private val mBinding get() = _binding

    companion object {
        private val TAG = EducationFragment::class.java.simpleName
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentEducationBinding.inflate(inflater, container, false)
        return mBinding?.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mResourceId = android.R.layout.simple_spinner_dropdown_item
        initInstances(); fetchAcademicData(); setListeners()
    }

    private fun initInstances() {
        mContact = AppInstance.getContact()
        mCourse = AppInstance.getCourse()
        mEducation = AppInstance.getEducation()
        mPerson = AppInstance.getPerson()
    }

    private fun fetchAcademicData() {
        HashMap<String, String>().let { header ->
            header["Accept"] = resources.getString(R.string.text_accept)
            Companions.makeJsonRequest(Request.Method.GET, Companions.courses(), null, header,
                object : Callback {
                    override fun onSuccess(result: JSONObject?) {
                        getSchools(result)
                    }

                    override fun onFailure(result: String?) {
                        Companions.debugLog(TAG, result)
                    }
                }
            )
            getSchoolYears(header); getBatches(header)
        }
    }

    private fun getSchools(result: JSONObject?) {
        val jsonArray = result?.getJSONArray("schools")
        val schools = ArrayList<School>()

        for (i in 0 until jsonArray!!.length()) {
            val jsonString = jsonArray.getJSONObject(i).toString()
            schools.add(Gson().fromJson(jsonString, School::class.java))
        }

        mSchoolAdapter = ArrayAdapter(context!!, mResourceId, schools)
        mBinding?.atvRegSchool?.threshold = 1
        mBinding?.atvRegSchool?.setAdapter(mSchoolAdapter)
        mBinding?.atvRegSchool?.onItemClickListener =
            AdapterView.OnItemClickListener { parent, _, position, _ ->
                mBinding?.atvRegDegree?.text = null
                mBinding?.atvRegMajor?.text = null
                getCourses(schools, (parent.adapter.getItem(position) as School).id)
            }
    }

    private fun getCourses(schools: ArrayList<School>, schoolId: String?) {
        val courseList = ArrayList<Course>()

        for (i in 0 until schools.size) {
            if (schools[i].id == schoolId) {
                courseList.addAll(schools[i].courses!!)
            }
        }

        mCourseAdapter = ArrayAdapter(context!!, mResourceId, courseList)
        mBinding?.atvRegDegree?.threshold = 1
        mBinding?.atvRegDegree?.setAdapter(mCourseAdapter)
        mBinding?.atvRegDegree?.onItemClickListener =
            AdapterView.OnItemClickListener { parent, _, position, _ ->
                mCourse?.code = (parent.adapter.getItem(position) as Course).code
                getMajors(courseList, (parent.adapter.getItem(position) as Course).id)
            }
    }

    private fun getMajors(courses: ArrayList<Course>, courseId: String?) {
        val set = HashSet<String>()
        val majorList = ArrayList<String>()

        for (i in 0 until courses.size) {
            set.add(courses[i].major!!)
        }

        majorList.addAll(set)

        mMajorAdapter = ArrayAdapter(context!!, mResourceId, majorList)
        mBinding?.atvRegMajor?.threshold = 1
        mBinding?.atvRegMajor?.setAdapter(mMajorAdapter)
        mBinding?.atvRegMajor?.onItemClickListener =
            AdapterView.OnItemClickListener { _, _, _, _ ->
                getDepartment(courses, courseId)
            }
    }

    private fun getDepartment(courses: ArrayList<Course>, courseId: String?) {
        val departments = ArrayList<Department>()

        for (i in 0 until courses.size) {
            if (courses[i].id == courseId) {
                departments.addAll(courses[i].departments!!)
            }
        }

        mEducation?.department = departments[0].name
    }

    private fun getSchoolYears(header: HashMap<String, String>) {
        Companions.makeCacheRequest(Request.Method.GET, Companions.schoolYears(), header,
            object : Callback {
                override fun onSuccess(result: JSONObject?) {
                    val jsonArray = result?.getJSONArray("school_years")
                    val schoolYears = ArrayList<String>()

                    for (i in 0 until jsonArray!!.length()) {
                        schoolYears.add(jsonArray.getJSONObject(i).getString("year"))
                    }

                    mSchoolYearAdapter =
                        ArrayAdapter(context!!, R.layout.item_spinner, schoolYears)
                    mSchoolYearAdapter?.setDropDownViewResource(mResourceId)
                    mBinding?.spnRegSchoolYear?.adapter = mSchoolYearAdapter
                    setValues(mSchoolYearAdapter)
                }

                override fun onFailure(result: String?) {
                    Companions.debugLog(TAG, result)
                }
            }
        )
    }

    private fun getBatches(header: HashMap<String, String>) {
        Companions.makeCacheRequest(Request.Method.GET, Companions.batches(), header,
            object : Callback {
                override fun onSuccess(result: JSONObject?) {
                    val jsonArray = result?.getJSONArray("batches")
                    val batches = ArrayList<String>()

                    for (i in 0 until jsonArray!!.length()) {
                        batches.add(jsonArray.getJSONObject(i).getString("month"))
                    }

                    mBatchAdapter = ArrayAdapter(context!!, R.layout.item_spinner, batches)
                    mBatchAdapter?.setDropDownViewResource(mResourceId)
                    mBinding?.spnRegBatch?.adapter = mBatchAdapter
                    setValues(mBatchAdapter)
                }

                override fun onFailure(result: String?) {
                    Companions.debugLog(TAG, result)
                }
            }
        )
    }

    private fun setValues(adapter: ArrayAdapter<String>?) {
        mBinding?.atvRegSchool?.setText(mEducation?.school)
        mBinding?.atvRegDegree?.setText(mCourse?.title)
        mBinding?.atvRegMajor?.setText(mCourse?.major)
        mBinding?.spnRegSchoolYear?.setSelection(adapter!!.getPosition(mEducation?.year))
        mBinding?.spnRegBatch?.setSelection(adapter!!.getPosition(mEducation?.month))
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun setListeners() {
        mBinding?.nav?.btnPrev?.setOnClickListener(this)
        mBinding?.nav?.btnNext?.setOnClickListener(this)
        mBinding?.atvRegSchool?.onFocusChangeListener = this
        mBinding?.atvRegDegree?.onFocusChangeListener = this
        mBinding?.layoutEducation?.setOnTouchListener(this)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.btnPrev -> {
                replaceFragment(BasicInfoFragment())
                (activity as RegisterActivity).getScrollView().fullScroll(ScrollView.FOCUS_UP)
            }
            R.id.btnNext -> saveEducation()
        }
    }

    override fun onFocusChange(v: View?, hasFocus: Boolean) {
        when (v!!.id) {
            R.id.atvRegSchool, R.id.atvRegDegree -> Companions.setBackgroundError(v, false)
        }
    }

    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
        v?.performClick()

        if (event?.action == MotionEvent.ACTION_DOWN) {
            if (v!!.id == R.id.layoutEducation)
                Companions.hideKeyboard(activity!!, v)

            return true
        }

        return false
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun saveEducation() {
        val school = mBinding?.atvRegSchool?.text.toString().trim()
        val degree =
            mBinding?.atvRegDegree?.text.toString().trim().replace("${mCourse?.code} - ", "")
        val major = mBinding?.atvRegMajor?.text.toString().trim()
        val year = mBinding?.spnRegSchoolYear?.selectedItem.toString().trim()
        val batch = mBinding?.spnRegBatch?.selectedItem.toString().trim()

        mEducation?.school = school; mCourse?.title = degree; mCourse?.major = major
        mEducation?.year = year; mEducation?.month = batch

        if (!isError(mBinding?.atvRegSchool, school, "School") &&
            !isError(mBinding?.atvRegDegree, degree, "Degree")
        ) {
            Companions.dialog(context, "Processing...")
            HashMap<String, String>().let { header ->
                header["Content-Type"] = resources.getString(R.string.text_accept)
                verify(header, setBody())
            }
        }
    }

    private fun setBody(): HashMap<String, String> {
        return HashMap<String, String>().also { body ->
            body["lastName"] = mPerson?.lastName!!
            body["firstName"] = mPerson?.firstName!!
            body["middleName"] = mPerson?.middleName!!
            body["gender"] = mPerson?.gender!!
            body["address"] = mContact?.toString()!!
            body["contactNumber"] = mContact?.contactNumber!!
            body["degree"] = mCourse?.title!!
            body["major"] = mCourse?.major!!
            body["department"] = mEducation?.department!!
            body["school"] = mEducation?.school!!
            body["schoolYear"] = mEducation?.year!!
            body["batch"] = mEducation?.month!!
        }
    }

    private fun verify(vararg maps: Map<String, String>) {
        Companions.makeJsonRequest(Request.Method.POST, Companions.verify(),
            JSONObject(maps[1]), maps[0], object : Callback {
                override fun onSuccess(result: JSONObject?) {
                    Companions.dialog.dismiss()
                    AlertDialog.Builder(context!!).let { builder ->
                        builder.setTitle("Verification Message")
                            .setMessage(result?.getString("message"))
                            .setCancelable(false).setPositiveButton("Confirm") { dialog, _ ->
                                replaceFragment(EmploymentFragment())
                                dialog.dismiss()
                            }
                        mDialog = builder.create()
                        mDialog.show()
                    }
                }

                override fun onFailure(result: String?) {
                    Companions.dialog.dismiss()
                    AlertDialog.Builder(context!!).let { builder ->
                        builder.setTitle("Verification Message").setMessage(result)
                            .setCancelable(false).setPositiveButton("Confirm") { dialog, _ ->
                                sendVerificationData(*maps)
                                dialog.dismiss()
                            }.setNegativeButton("Cancel") { dialog, _ ->
                                dialog.dismiss()
                            }
                        mDialog = builder.create()
                        mDialog.show()
                    }
                }
            }
        )
    }

    private fun sendVerificationData(vararg maps: Map<String, String>) {
        Companions.dialog(context, "Processing...")
        Companions.makeJsonRequest(Request.Method.POST, Companions.verification(),
            JSONObject(maps[1]), maps[0], object : Callback {
                override fun onSuccess(result: JSONObject?) {
                    Companions.dialog.dismiss()
                    AlertDialog.Builder(context!!).let { builder ->
                        builder.setTitle("Confirmation Message")
                            .setMessage(result?.getString("message"))
                            .setCancelable(false).setPositiveButton("OK") { dialog, _ ->
                                dialog.dismiss()
                            }
                        mDialog = builder.create()
                        mDialog.show()
                    }
                }

                override fun onFailure(result: String?) {
                    Companions.dialog.dismiss()
                    AlertDialog.Builder(context!!).let { builder ->
                        builder.setTitle("Message").setMessage(result)
                            .setCancelable(false).setPositiveButton("Confirm") { dialog, _ ->
                                dialog.dismiss()
                            }
                        mDialog = builder.create()
                        mDialog.show()
                    }
                }
            }
        )
    }

    private fun replaceFragment(otherFragment: Fragment) =
        Companions.replaceFragment(activity!!, otherFragment)

    private fun isError(view: View?, input: String?, tag: String?) =
        Companions.isError(view, input, tag)
}
