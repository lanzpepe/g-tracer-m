package com.gtracer.capstone.services

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.graphics.Color
import android.os.Build
import androidx.core.app.NotificationCompat
import com.gtracer.capstone.objects.Companions
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import org.json.JSONException
import org.json.JSONObject
import kotlin.random.Random

class FirebaseMessaging : FirebaseMessagingService() {

    private var mMessageType: String = ""

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        if (remoteMessage.data.isNotEmpty()) {
            mMessageType = "json"
        }
        if (remoteMessage.notification != null) {
            mMessageType = "message"
        }
        showNotification(remoteMessage.data.toString())
    }

    private fun showNotification(body: String) {
        var title = ""; var message = ""
        val manager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val channelId = "${this.packageName}.test"

        if (mMessageType == "json") {
            try {
                val jsonObject = JSONObject(body)
                title = jsonObject.getJSONObject("data").getString("title")
                message = jsonObject.getJSONObject("data").getString("message")
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }
        else {
            message = body
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel(channelId, "Notification",
                NotificationManager.IMPORTANCE_HIGH).let { channel ->
                channel.description = "G-Tracer channel"
                channel.enableLights(true)
                channel.lightColor = Color.BLUE
                channel.vibrationPattern = longArrayOf(0, 1000, 500, 1000)
                channel.enableVibration(true)
                channel.lockscreenVisibility = Notification.VISIBILITY_PUBLIC
                manager.createNotificationChannel(channel)
            }
        }

        NotificationCompat.Builder(this, channelId).let { builder ->
            /*builder.setAutoCancel(true)
            builder.setDefaults(Notification.DEFAULT_ALL)
            builder.setWhen(System.currentTimeMillis())*/
            builder.setSmallIcon(android.R.drawable.sym_action_email)
            builder.setContentTitle(title)
            builder.setContentText(message)
            builder.priority = NotificationCompat.PRIORITY_DEFAULT
            builder.setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
            manager.notify(Random.nextInt(), builder.build())
        }
    }

    override fun onNewToken(s: String) {
        super.onNewToken(s)
        Companions.debugLog("FCM Token", s)
    }
}