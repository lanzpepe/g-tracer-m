package com.gtracer.capstone.listeners

import android.content.Context
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class RecyclerItemClickListener(context: Context?, itemClickListener: OnItemClickListener?) :
    RecyclerView.OnItemTouchListener, GestureDetector.SimpleOnGestureListener() {

    private val mGestureDetector = GestureDetector(context, this)
    private val mListener = itemClickListener

    override fun onSingleTapUp(e: MotionEvent?): Boolean = true

    override fun onTouchEvent(rv: RecyclerView, e: MotionEvent) {}

    override fun onInterceptTouchEvent(rv: RecyclerView, e: MotionEvent): Boolean {
        val child = rv.findChildViewUnder(e.x, e.y)

        if (mListener != null && child != null && mGestureDetector.onTouchEvent(e)) {
            mListener.onItemClick(child, rv.getChildAdapterPosition(child))
            return true
        }

        return false
    }

    override fun onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {}

    interface OnItemClickListener {
        fun onItemClick(view: View?, position: Int)
    }
}