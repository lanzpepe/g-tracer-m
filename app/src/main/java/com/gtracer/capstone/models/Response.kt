package com.gtracer.capstone.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class Response(
    @SerializedName("response_id") var responseId: String? = null,
    @SerializedName("job_position") var jobPosition: String? = null,
    @SerializedName("date_employed") var dateEmployed: String? = null,
    var remarks: String? = null, var respondent: Respondent? = null,
    var company: Company? = null
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readString(), parcel.readString(),
        parcel.readString(), parcel.readString(),
        parcel.readParcelable(Respondent::class.java.classLoader),
        parcel.readParcelable(Company::class.java.classLoader)
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(responseId)
        parcel.writeString(jobPosition)
        parcel.writeString(dateEmployed)
        parcel.writeString(remarks)
        parcel.writeParcelable(respondent, flags)
        parcel.writeParcelable(company, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Response> {
        override fun createFromParcel(parcel: Parcel): Response {
            return Response(parcel)
        }

        override fun newArray(size: Int): Array<Response?> {
            return arrayOfNulls(size)
        }
    }
}