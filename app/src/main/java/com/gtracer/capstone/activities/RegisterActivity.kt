package com.gtracer.capstone.activities

import android.content.Intent
import android.os.Bundle
import android.view.WindowManager
import android.widget.ScrollView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.gtracer.capstone.R
import com.gtracer.capstone.databinding.ActivityRegisterBinding
import com.gtracer.capstone.fragments.BasicInfoFragment
import com.gtracer.capstone.objects.Companions

class RegisterActivity : AppCompatActivity() {

    private lateinit var mBinding: ActivityRegisterBinding
    private lateinit var mDialog: AlertDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityRegisterBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        privacyDialog()
    }

    private fun privacyDialog() {
        AlertDialog.Builder(this).let { builder ->
            builder.setTitle("Privacy Statement")
                .setMessage(resources.getString(R.string.text_privacy))
                .setPositiveButton("Accept", null)
                .setNegativeButton("Decline", null)
            mDialog = builder.create(); mDialog.show()
            mDialog.getButton(AlertDialog.BUTTON_POSITIVE)?.setOnClickListener {
                mDialog.dismiss()
                Companions.addFragment(this, BasicInfoFragment(), "info")
            }
            mDialog.getButton(AlertDialog.BUTTON_NEGATIVE)?.setOnClickListener {
                mDialog.dismiss()
                Intent(this, LoginActivity::class.java).also { intent ->
                    startActivity(intent); finish()
                    Companions.fadeTransition(this)
                }
            }
        }
    }

    fun getScrollView(): ScrollView = mBinding.scrollView
}