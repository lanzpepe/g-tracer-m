package com.gtracer.capstone.activities

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Request
import com.gtracer.capstone.R
import com.gtracer.capstone.adapters.TransactionListAdapter
import com.gtracer.capstone.models.Reward
import com.gtracer.capstone.objects.Companions
import com.gtracer.capstone.objects.Constant
import com.gtracer.capstone.singletons.AppVolley.Callback
import com.google.gson.Gson
import com.gtracer.capstone.databinding.ActivityTransactionBinding
import org.json.JSONObject

class TransactionActivity : AppCompatActivity() {

    private var mUserRewards: ArrayList<Reward>? = null
    private var mAdapter: TransactionListAdapter? = null
    private lateinit var mBinding: ActivityTransactionBinding

    companion object {
        private val TAG = TransactionActivity::class.java.simpleName
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityTransactionBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        setSupportActionBar(mBinding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(true)
        mUserRewards = ArrayList()
        fetchUserReward()
        mAdapter = TransactionListAdapter(this, mUserRewards)
        mBinding.rvHistory.setHasFixedSize(true)
        mBinding.rvHistory.adapter = mAdapter
    }

    private fun fetchUserReward() {
        HashMap<String, String>().let { header ->
            header["Accept"] = resources.getString(R.string.text_accept)
            header["Authorization"] =
                resources.getString(R.string.text_authorization, Companions.getAccessToken())
            Companions.makeJsonRequest(
                Request.Method.GET, Companions.user(), null, header, object : Callback {
                    override fun onSuccess(result: JSONObject?) {
                        populateUserReward(result)
                    }

                    override fun onFailure(result: String?) {
                        Companions.debugLog(TAG, result)
                    }
                }
            )
        }
    }

    private fun populateUserReward(result: JSONObject?) {
        val jsonArray = result?.getJSONObject(Constant.USER)?.getJSONArray("rewards")

        mUserRewards?.clear()

        for (i in 0 until jsonArray!!.length()) {
            mUserRewards?.add(
                Gson().fromJson(
                    jsonArray.getJSONObject(i).toString(), Reward::class.java
                )
            )
        }

        mAdapter?.notifyDataSetChanged()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onResume() {
        super.onResume()
        fetchUserReward()
    }
}
