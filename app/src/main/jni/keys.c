#include <jni.h>

JNIEXPORT jstring JNICALL
Java_com_gtracer_capstone_singletons_AppInstance_00024Companion_getGoogleMapsApiKey(
        JNIEnv *env, jobject object) {
    return (*env)->NewStringUTF(env, "QUl6YVN5QVZlWmRFQWFEeXRVSXk2NnNVYmxkeldEMWZkTENzWExR");
}

JNIEXPORT jstring JNICALL
Java_com_gtracer_capstone_singletons_AppInstance_00024Companion_getLinkedInClientKey(
        JNIEnv *env, jobject object) {
    return (*env)->NewStringUTF(env, "ODZzeThoMnpkbGpoNXg=");
}

JNIEXPORT jstring JNICALL
Java_com_gtracer_capstone_singletons_AppInstance_00024Companion_getLinkedInSecretKey(
        JNIEnv *env, jobject object) {
    return (*env)->NewStringUTF(env, "Z1VmSDBxZmR4dGFKZHdZbw==");
}