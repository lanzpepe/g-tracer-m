package com.gtracer.capstone.singletons

import android.content.Context
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.VolleyError
import com.android.volley.toolbox.HurlStack
import com.android.volley.toolbox.Volley
import org.json.JSONObject

class AppVolley(var context: Context) {

    companion object {
        @Volatile
        private var sInstance: AppVolley? = null

        fun getInstance(context: Context) = sInstance ?: synchronized(this) {
            sInstance ?: AppVolley(context).also {
                sInstance = it
            }
        }
    }

    private val requestQueue: RequestQueue by lazy {
        Volley.newRequestQueue(context.applicationContext, HurlStack())
    }

    fun <T> addToRequestQueue(request: Request<T>) {
        requestQueue.add(request)
    }

    interface Callback {
        fun onSuccess(result: JSONObject?)
        fun onFailure(result: String?)
    }
}