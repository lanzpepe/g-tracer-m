package com.gtracer.capstone.models

import android.os.Parcel
import android.os.Parcelable

data class Confirmed(
    var id: String? = null, var status: String? = null,
    var remarks: String? = null, var employment: Employment? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readParcelable(Employment::class.java.classLoader)
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(status)
        parcel.writeString(remarks)
        parcel.writeParcelable(employment, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Confirmed> {
        override fun createFromParcel(parcel: Parcel): Confirmed {
            return Confirmed(parcel)
        }

        override fun newArray(size: Int): Array<Confirmed?> {
            return arrayOfNulls(size)
        }
    }
}