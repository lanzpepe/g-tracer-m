package com.gtracer.capstone.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class Reward(var id: String?, var name: String?, var description: String?, var points: Int,
                  @SerializedName("image_uri") var imageUri: String?,
                  var pivot: UserReward? = null, var admins: ArrayList<Admin>?
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readInt(),
        parcel.readString(),
        parcel.readParcelable(UserReward::class.java.classLoader),
        parcel.createTypedArrayList(Admin.CREATOR)
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(name)
        parcel.writeString(description)
        parcel.writeInt(points)
        parcel.writeString(imageUri)
        parcel.writeParcelable(pivot, flags)
        parcel.writeTypedList(admins)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Reward> {
        override fun createFromParcel(parcel: Parcel): Reward {
            return Reward(parcel)
        }

        override fun newArray(size: Int): Array<Reward?> {
            return arrayOfNulls(size)
        }
    }
}