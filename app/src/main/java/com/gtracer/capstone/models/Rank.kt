package com.gtracer.capstone.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class Rank(var id: String? = null, var name: String? = null,
                @SerializedName("exp_point") var points: Long = 0
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readLong()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(name)
        parcel.writeLong(points)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Rank> {
        override fun createFromParcel(parcel: Parcel): Rank {
            return Rank(parcel)
        }

        override fun newArray(size: Int): Array<Rank?> {
            return arrayOfNulls(size)
        }
    }
}