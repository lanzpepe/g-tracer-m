﻿package com.gtracer.capstone.activities

import android.content.Intent
import android.os.Bundle
import android.util.Base64
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Request
import com.gtracer.capstone.MainActivity
import com.gtracer.capstone.R
import com.gtracer.capstone.databinding.ActivityLoginBinding
import com.gtracer.capstone.models.Contact
import com.gtracer.capstone.models.Person
import com.gtracer.capstone.models.User
import com.gtracer.capstone.objects.Companions
import com.gtracer.capstone.objects.Constant
import com.gtracer.capstone.objects.PrefsHelper
import com.gtracer.capstone.singletons.AppInstance
import com.gtracer.capstone.singletons.AppVolley.Callback
import com.kromer.linkedinsdk.Linkedin
import com.kromer.linkedinsdk.data.enums.ErrorCode
import com.kromer.linkedinsdk.data.model.LinkedInUser
import com.kromer.linkedinsdk.utils.Constants
import org.json.JSONObject

class LoginActivity : AppCompatActivity(), Callback {

    private var mContact: Contact? = null
    private var mPerson: Person? = null
    private var mUser: User? = null
    private lateinit var mClientId: String
    private lateinit var mClientSecret: String
    private lateinit var mRedirectUri: String
    private lateinit var mState: String
    private lateinit var mBinding: ActivityLoginBinding

    companion object {
        private val TAG = LoginActivity::class.java.simpleName
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        Companions.setStatusBarColor(this, window)
        mClientId = decode(AppInstance.getLinkedInClientKey())
        mClientSecret = decode(AppInstance.getLinkedInSecretKey())
        mRedirectUri = resources.getString(R.string.li_redirect_uri)
        mState = resources.getString(R.string.li_state)
        initInstances();
        mBinding.btnLinkedIn.setOnClickListener { loginWithLinkedIn() }
    }

    private fun initInstances() {
        mContact = AppInstance.getContact()
        mPerson = AppInstance.getPerson()
        mUser = AppInstance.getUser()
    }

    private fun loginWithLinkedIn() {
        Linkedin.signIn(this, mClientId, mClientSecret, mRedirectUri, mState)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Constants.REQ_CODE_SIGN_IN && data != null) {
            if (resultCode == Constants.SUCCESS) {
                val user = data.getParcelableExtra<LinkedInUser>(Constants.USER)

                PrefsHelper.writeString(Constant.PROVIDER_ID, user?.id)
                PrefsHelper.writeString(Constant.PROVIDER_NAME, "linkedin.com")

                mPerson?.lastName = user?.lastName; mPerson?.firstName = user?.firstName
                mUser?.imageUri = user?.profilePicture; mContact?.email = user?.emailAddress

                Companions.toast("Authenticated successfully.")

                HashMap<String, String>().let { header ->
                    header["Content-Type"] = resources.getString(R.string.text_accept)
                    HashMap<String, String>().let { body ->
                        body["provider_id"] = Companions.getProviderId()!!
                        body["provider_name"] = Companions.getProviderName()!!
                        checkExistingAccount(header, body)
                    }
                }
            } else {
                when (data.getIntExtra(Constants.ERROR_TYPE, 0)) {
                    ErrorCode.ERROR_NO_INTERNET -> {
                        Companions.toast("Network error. Please check your connection.")
                    }
                    ErrorCode.ERROR_USER_CANCELLED -> {
                        Companions.toast("The web operation has been canceled by the user.")
                    }
                    ErrorCode.ERROR_OTHER -> {
                        Companions.toast("Invalid request. Unable to authenticate user.")
                    }
                }
            }
        }
    }

    private fun checkExistingAccount(vararg maps: Map<String, String>) {
        Companions.makeJsonRequest(
            Request.Method.POST, Companions.login(), JSONObject(maps[1]), maps[0], this
        )
        mBinding.btnLinkedIn.visibility = View.GONE
        mBinding.progressBar.visibility = View.VISIBLE
    }

    override fun onSuccess(result: JSONObject?) {
        val accessToken = result?.getString(Constant.ACCESS_TOKEN)
        val refreshToken = result?.getString(Constant.REFRESH_TOKEN)
        val expiresIn = result?.getLong(Constant.EXPIRES_IN)

        PrefsHelper.writeString(Constant.ACCESS_TOKEN, accessToken)
        PrefsHelper.writeString(Constant.REFRESH_TOKEN, refreshToken)
        PrefsHelper.writeLong(Constant.EXPIRES_IN, expiresIn!!)

        Intent(this@LoginActivity, MainActivity::class.java).also {
            startActivity(it); finish()
            Companions.slideTransition(this@LoginActivity)
        }
    }

    override fun onFailure(result: String?) {
        Intent(this@LoginActivity, RegisterActivity::class.java).also {
            startActivity(it); finish()
            Companions.slideTransition(this@LoginActivity)
        }
    }

    private fun decode(value: String?): String = String(Base64.decode(value, Base64.DEFAULT))
}