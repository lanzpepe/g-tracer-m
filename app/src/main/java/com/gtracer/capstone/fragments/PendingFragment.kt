package com.gtracer.capstone.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.android.volley.Request
import com.gtracer.capstone.R
import com.gtracer.capstone.activities.ResponseActivity
import com.gtracer.capstone.adapters.SavedGraduatesAdapter
import com.gtracer.capstone.listeners.RecyclerItemClickListener
import com.gtracer.capstone.models.Graduate
import com.gtracer.capstone.models.User
import com.gtracer.capstone.objects.Companions
import com.gtracer.capstone.objects.Constant
import com.gtracer.capstone.singletons.AppVolley.Callback
import com.google.gson.Gson
import com.gtracer.capstone.databinding.FragmentPendingBinding
import org.json.JSONObject

class PendingFragment : Fragment(), RecyclerItemClickListener.OnItemClickListener {

    private var _binding: FragmentPendingBinding? = null
    private var mUser: User? = null
    private var mAdapter: SavedGraduatesAdapter? = null
    private var mSavedGraduatesList: ArrayList<Graduate>? = null
    private val mBinding get() = _binding

    companion object {
        private val TAG = PendingFragment::class.java.simpleName
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentPendingBinding.inflate(inflater, container, false)
        return mBinding?.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mSavedGraduatesList = ArrayList()
        fetchPendingGraduates()
        mAdapter = SavedGraduatesAdapter(context, mSavedGraduatesList)
        mBinding?.rvPending?.setHasFixedSize(true)
        mBinding?.rvPending?.adapter = mAdapter
        mBinding?.rvPending?.addOnItemTouchListener(RecyclerItemClickListener(context, this))
    }

    override fun onItemClick(view: View?, position: Int) {
        Intent(context, ResponseActivity::class.java).also {
            it.putExtra(Constant.GRADUATE, mSavedGraduatesList!![position])
            it.putExtra(Constant.USER_ID, mUser?.userId)
            startActivityForResult(it, Constant.RESPONSE_REQUEST_CODE)
        }
    }

    private fun fetchPendingGraduates() {
        HashMap<String, String>().let { header ->
            header["Accept"] = resources.getString(R.string.text_accept)
            header["Authorization"] =
                resources.getString(R.string.text_authorization, Companions.getAccessToken())
            Companions.makeJsonRequest(Request.Method.GET, Companions.user(), null, header,
                object : Callback {
                    override fun onSuccess(result: JSONObject?) {
                        val userJson = result?.getJSONObject(Constant.USER)
                        val graduatesJson = userJson?.getJSONArray(Constant.GRADUATES)

                        mUser = Gson().fromJson(userJson?.toString(), User::class.java)
                        mSavedGraduatesList?.clear()

                        for (i in 0 until graduatesJson!!.length()) {
                            val obj = graduatesJson.getJSONObject(i)
                            val graduate = Gson().fromJson(obj.toString(), Graduate::class.java)

                            if (graduate.respondent?.responseId == null) {
                                if (!mSavedGraduatesList!!.contains(graduate)) {
                                    mSavedGraduatesList?.add(graduate)
                                }
                            }
                        }

                        mAdapter?.notifyDataSetChanged()
                    }

                    override fun onFailure(result: String?) {
                        Companions.debugLog(TAG, result)
                    }
                }
            )
        }
    }

    override fun onResume() {
        super.onResume()
        fetchPendingGraduates()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
