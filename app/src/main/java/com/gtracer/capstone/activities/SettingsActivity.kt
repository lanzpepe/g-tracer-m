package com.gtracer.capstone.activities

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.ArrayAdapter
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Request
import com.gtracer.capstone.R
import com.gtracer.capstone.models.*
import com.gtracer.capstone.objects.Companions
import com.gtracer.capstone.objects.Constant
import com.gtracer.capstone.singletons.AppVolley.Callback
import com.google.gson.Gson
import com.gtracer.capstone.databinding.ActivitySettingsBinding
import org.json.JSONArray
import org.json.JSONObject

class SettingsActivity : AppCompatActivity(), View.OnClickListener {

    private var mSchoolAdapter: ArrayAdapter<School>? = null
    private var mDepartmentAdapter: ArrayAdapter<Department>? = null
    private var mCourseAdapter: ArrayAdapter<Course>? = null
    private var mSchoolYearAdapter: ArrayAdapter<String>? = null
    private var mBatchAdapter: ArrayAdapter<String>? = null
    private var mResourceId: Int = 0
    private lateinit var mUser: User
    private lateinit var mDialog: AlertDialog
    private lateinit var mBinding: ActivitySettingsBinding

    companion object {
        private val TAG = SettingsActivity::class.java.simpleName
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivitySettingsBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        setListeners(); setSupportActionBar(mBinding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(true)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        mResourceId = android.R.layout.simple_list_item_single_choice
        mUser = intent.getParcelableExtra(Constant.USER)!!
        fetchAcademicData(); setPreferences()
    }

    private fun setPreferences() {
        mBinding.tvPrefsSchool.text = mUser.preference?.school
        mBinding.tvPrefsDepartment.text = mUser.preference?.department
        mBinding.tvPrefsDegree.text = mUser.preference?.degree
        mBinding.tvPrefsMajor.text = mUser.preference?.major
        mBinding.tvPrefsSchoolYear.text = mUser.preference?.schoolYear
        mBinding.tvPrefsBatch.text = mUser.preference?.batch
    }

    private fun fetchAcademicData() {
        HashMap<String, String>().let { header ->
            header["Accept"] = resources.getString(R.string.text_accept)
            Companions.makeJsonRequest(Request.Method.GET, Companions.schools(), null, header,
                object : Callback {
                    override fun onSuccess(result: JSONObject?) {
                        getSchools(result?.getJSONArray("schools"))
                    }

                    override fun onFailure(result: String?) {
                        Companions.debugLog(TAG, result)
                    }
                }
            )
            getSchoolYears(header); getBatches(header)
        }
    }

    private fun getSchools(jsonArray: JSONArray?) {
        val schools = ArrayList<School>()

        for (i in 0 until jsonArray!!.length()) {
            val jsonString = jsonArray.getJSONObject(i).toString()
            schools.add(Gson().fromJson(jsonString, School::class.java))
        }

        mSchoolAdapter = ArrayAdapter(this, mResourceId, schools)
        mBinding.tvPrefsSchool.setOnClickListener {
            AlertDialog.Builder(this).let { builder ->
                builder.setTitle("Select School")
                    .setSingleChoiceItems(mSchoolAdapter, -1) { dialog, which ->
                        getDepartments(schools, mSchoolAdapter?.getItem(which)?.id)
                        mBinding.tvPrefsSchool.text = mSchoolAdapter?.getItem(which)?.name
                        mBinding.tvPrefsDepartment.text = null; mBinding.tvPrefsDegree.text = null
                        mBinding.tvPrefsMajor.text = null
                        dialog.dismiss()
                    }
                mDialog = builder.create()
                mDialog.show()
            }
        }
    }

    private fun getDepartments(schools: ArrayList<School>, schoolId: String?) {
        val departments = ArrayList<Department>()

        for (i in 0 until schools.size) {
            if (schools[i].id == schoolId) {
                for (j in 0 until schools[i].departments!!.size) {
                    departments.add(schools[i].departments!![j])
                }
            }
        }

        mDepartmentAdapter = ArrayAdapter(this, mResourceId, departments)
        mBinding.tvPrefsDepartment.setOnClickListener {
            AlertDialog.Builder(this).let { builder ->
                builder.setTitle("Select Department")
                    .setSingleChoiceItems(mDepartmentAdapter, -1) { dialog, which ->
                        getCourses(departments, mDepartmentAdapter?.getItem(which)?.id)
                        mBinding.tvPrefsDepartment.text = mDepartmentAdapter?.getItem(which)?.name
                        mBinding.tvPrefsDegree.text = null; mBinding.tvPrefsMajor.text = null
                        dialog.dismiss()
                    }
                mDialog = builder.create()
                mDialog.show()
            }
        }
    }

    private fun getCourses(departments: ArrayList<Department>, deptId: String?) {
        val courses = ArrayList<Course>()

        for (i in 0 until departments.size) {
            if (departments[i].id == deptId) {
                for (j in 0 until departments[i].courses!!.size) {
                    courses.add(departments[i].courses!![j])
                }
            }
        }

        mCourseAdapter = ArrayAdapter(this, mResourceId, courses)
        mBinding.tvPrefsDegree.setOnClickListener {
            AlertDialog.Builder(this).let { builder ->
                builder.setTitle("Select Degree Program")
                    .setSingleChoiceItems(mCourseAdapter, -1) { dialog, which ->
                        mBinding.tvPrefsDegree.text = mCourseAdapter?.getItem(which)?.title
                        mBinding.tvPrefsMajor.text = mCourseAdapter?.getItem(which)?.major
                        dialog.dismiss()
                    }
                mDialog = builder.create()
                mDialog.show()
            }
        }
    }

    private fun getSchoolYears(header: HashMap<String, String>) {
        Companions.makeJsonRequest(Request.Method.GET, Companions.schoolYears(), null, header,
            object : Callback {
                override fun onSuccess(result: JSONObject?) {
                    val jsonArray = result?.getJSONArray("school_years")
                    val years = ArrayList<String>()

                    for (i in 0 until jsonArray!!.length()) {
                        years.add(jsonArray.getJSONObject(i).getString("year"))
                    }

                    mSchoolYearAdapter =
                        ArrayAdapter(this@SettingsActivity, mResourceId, years)
                    mBinding.tvPrefsSchoolYear.setOnClickListener {
                        AlertDialog.Builder(this@SettingsActivity).let { builder ->
                            builder.setTitle("Select School Year")
                                .setSingleChoiceItems(mSchoolYearAdapter, -1) { dialog, which ->
                                    mBinding.tvPrefsSchoolYear.text =
                                        mSchoolYearAdapter?.getItem(which)
                                    dialog.dismiss()
                                }
                            mDialog = builder.create()
                            mDialog.show()
                        }
                    }
                }

                override fun onFailure(result: String?) {
                    Companions.debugLog(TAG, result)
                }
            }
        )
    }

    private fun getBatches(header: HashMap<String, String>) {
        Companions.makeJsonRequest(Request.Method.GET, Companions.batches(), null, header,
            object : Callback {
                override fun onSuccess(result: JSONObject?) {
                    val jsonArray = result?.getJSONArray("batches")
                    val batches = ArrayList<String>()

                    for (i in 0 until jsonArray!!.length()) {
                        batches.add(jsonArray.getJSONObject(i).getString("month"))
                    }

                    mBatchAdapter =
                        ArrayAdapter(this@SettingsActivity, mResourceId, batches)
                    mBinding.tvPrefsBatch.setOnClickListener {
                        AlertDialog.Builder(this@SettingsActivity).let { builder ->
                            builder.setTitle("Select Batch")
                                .setSingleChoiceItems(mBatchAdapter, -1) { dialog, which ->
                                    mBinding.tvPrefsBatch.text = mBatchAdapter?.getItem(which)
                                    dialog.dismiss()
                                }
                            mDialog = builder.create()
                            mDialog.show()
                        }
                    }
                }

                override fun onFailure(result: String?) {
                    Companions.debugLog(TAG, result)
                }
            }
        )
    }

    private fun setListeners() {
        mBinding.btnPrefsSave.setOnClickListener(this)
        mBinding.btnLogout.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.btnPrefsSave -> {
                savePreferences()
            }
            R.id.btnLogout -> {
                val dialog: AlertDialog

                AlertDialog.Builder(this).let {
                    it.setMessage("Do you want to log out?").setCancelable(false)
                        .setPositiveButton("OK", null).setNegativeButton("Cancel", null)
                    dialog = it.create(); dialog.show()
                }

                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener {
                    logout(); dialog.dismiss()
                }
            }
        }
    }

    private fun logout() {
        HashMap<String, String>().let { header ->
            header["Accept"] = "application/json"
            header["Authorization"] = "Bearer ${Companions.getAccessToken()}"
            Companions.makeJsonRequest(Request.Method.GET, Companions.logout(), null, header,
                object : Callback {
                    override fun onSuccess(result: JSONObject?) {
                        Intent(this@SettingsActivity, LoginActivity::class.java).also {
                            it.addFlags(
                                Intent.FLAG_ACTIVITY_CLEAR_TOP or
                                        Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                            )
                            startActivity(it)
                        }
                        Companions.toast(result?.getString("message"))
                    }

                    override fun onFailure(result: String?) {
                        Companions.debugLog(TAG, result)
                    }
                }
            )
        }
    }

    private fun savePreferences() {
        Companions.dialog(this, "Processing...")
        HashMap<String, String>().let { header ->
            header["Content-Type"] = resources.getString(R.string.text_accept)
            header["Authorization"] =
                resources.getString(R.string.text_authorization, Companions.getAccessToken())
            HashMap<String, String>().let { body ->
                body["school"] = mBinding.tvPrefsSchool.text.toString().trim()
                body["department"] = mBinding.tvPrefsDepartment.text.toString().trim()
                body["degree"] = mBinding.tvPrefsDegree.text.toString().trim()
                body["major"] = mBinding.tvPrefsMajor.text.toString().trim()
                body["schoolYear"] = mBinding.tvPrefsSchoolYear.text.toString().trim()
                body["batch"] = mBinding.tvPrefsBatch.text.toString().trim()
                val maps: Array<Map<String, String>> = arrayOf(body, header)
                Companions.makeJsonRequest(Request.Method.POST, Companions.preference(),
                    JSONObject(maps[0]), maps[1], object : Callback {
                        override fun onSuccess(result: JSONObject?) {
                            Companions.dialog.dismiss()
                            AlertDialog.Builder(this@SettingsActivity).let { builder ->
                                builder.setTitle("Confirmation")
                                    .setMessage(result?.getString("message"))
                                    .setCancelable(false)
                                    .setPositiveButton("OK") { dialog, _ ->
                                        dialog.dismiss()
                                    }
                                mDialog = builder.create()
                                mDialog.show()
                            }
                        }

                        override fun onFailure(result: String?) {
                            Companions.dialog.dismiss()
                            AlertDialog.Builder(this@SettingsActivity).let { builder ->
                                builder.setTitle("Confirmation")
                                    .setMessage(result)
                                    .setCancelable(false)
                                    .setPositiveButton("OK") { dialog, _ ->
                                        dialog.dismiss()
                                    }
                                mDialog = builder.create()
                                mDialog.show()
                            }
                        }
                    }
                )
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home)
            finish()

        return super.onOptionsItemSelected(item)
    }
}
