package com.gtracer.capstone.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class User(
    @SerializedName("user_id") var userId: String? = null,
    @SerializedName("image_uri") var imageUri: String? = null,
    @SerializedName("device_token") var token: String? = null,
    @SerializedName("provider_accounts") var accounts: ArrayList<ProviderAccount>? = null,
    var person: Person? = null, var graduate: Graduate? = null, var respondent: Respondent? = null,
    var achievement: Achievement? = null, var preference: Preference? = null,
    var employments: ArrayList<Employment>? = null, var graduates: ArrayList<Graduate>? = null,
    var responses: ArrayList<Response>? = null, var rewards: ArrayList<Reward>? = null
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readString(), parcel.readString(), parcel.readString(),
        parcel.createTypedArrayList(ProviderAccount),
        parcel.readParcelable(Person::class.java.classLoader),
        parcel.readParcelable(Graduate::class.java.classLoader),
        parcel.readParcelable(Respondent::class.java.classLoader),
        parcel.readParcelable(Achievement::class.java.classLoader),
        parcel.readParcelable(Preference::class.java.classLoader),
        parcel.createTypedArrayList(Employment.CREATOR),
        parcel.createTypedArrayList(Graduate.CREATOR),
        parcel.createTypedArrayList(Response.CREATOR),
        parcel.createTypedArrayList(Reward.CREATOR)
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(userId)
        parcel.writeString(imageUri)
        parcel.writeString(token)
        parcel.writeTypedList(accounts)
        parcel.writeParcelable(person, flags)
        parcel.writeParcelable(graduate, flags)
        parcel.writeParcelable(respondent, flags)
        parcel.writeParcelable(achievement, flags)
        parcel.writeParcelable(preference, flags)
        parcel.writeTypedList(employments)
        parcel.writeTypedList(graduates)
        parcel.writeTypedList(responses)
        parcel.writeTypedList(rewards)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<User> {
        override fun createFromParcel(parcel: Parcel): User {
            return User(parcel)
        }

        override fun newArray(size: Int): Array<User?> {
            return arrayOfNulls(size)
        }
    }
}