package com.gtracer.capstone.threads

import android.graphics.drawable.Drawable
import android.os.AsyncTask
import android.view.View
import android.widget.ImageView
import com.gtracer.capstone.singletons.AppInstance
import com.gtracer.capstone.utils.GlideApp
import java.net.HttpURLConnection
import java.net.URL

class ImageTask(var view: View, var url: String?, var uri: Drawable) : AsyncTask<String, Unit, Boolean>() {

    private var mContext = AppInstance.getContext()

    override fun doInBackground(vararg params: String?): Boolean {
        var connection: HttpURLConnection? = null

        try {
            val url = URL(params[0])

            connection = url.openConnection() as HttpURLConnection
            connection.connect()

            return connection.responseCode == HttpURLConnection.HTTP_OK
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            connection?.disconnect()
        }

        return false
    }

    override fun onPostExecute(result: Boolean?) {
        GlideApp.with(mContext).load(if (result!!) url else uri).placeholder(uri)
            .into(view as ImageView)
    }
}