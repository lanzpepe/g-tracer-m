package com.gtracer.capstone.utils

import com.android.volley.*
import com.android.volley.toolbox.HttpHeaderParser

open class CacheRequest(
    method: Int, url: String?, listener: Response.Listener<NetworkResponse>,
    errorListener: Response.ErrorListener
) : Request<NetworkResponse>(method, url, errorListener) {

    private var mListener = listener
    private var mErrorListener = errorListener

    override fun parseNetworkResponse(response: NetworkResponse?): Response<NetworkResponse> {
        // cache entry expires in 30 days
        val cacheExpired = 720 * 60 * 60 * 1000L
        val softExpire = System.currentTimeMillis()
        val ttl = softExpire + cacheExpired
        var headerValue = response?.headers!!["Date"]
        var cacheEntry = HttpHeaderParser.parseCacheHeaders(response)

        if (cacheEntry == null)
            cacheEntry = Cache.Entry()

        cacheEntry.data = response.data
        cacheEntry.softTtl = softExpire
        cacheEntry.ttl = ttl

        if (headerValue != null)
            cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue)

        headerValue = response.headers!!["Last-Modified"]

        if (headerValue != null)
            cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue)

        cacheEntry.responseHeaders = response.headers

        return Response.success(response, cacheEntry)
    }

    override fun deliverResponse(response: NetworkResponse?) {
        mListener.onResponse(response)
    }

    override fun deliverError(error: VolleyError?) {
        mErrorListener.onErrorResponse(error)
    }
}