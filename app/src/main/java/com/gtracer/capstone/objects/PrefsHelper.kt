package com.gtracer.capstone.objects

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit

object PrefsHelper {

    private lateinit var mSharedPreferences: SharedPreferences
    private const val KEY_PREFERENCES = "com.example.chua.gtracer.preferences"

    fun init(context: Context) {
        mSharedPreferences = context.getSharedPreferences(KEY_PREFERENCES, Context.MODE_PRIVATE)
    }

    fun readLong(key: String?, value: Long): Long = mSharedPreferences.getLong(key, value)

    fun readString(key: String?, value: String?): String? = mSharedPreferences.getString(key, value)

    fun readBoolean(key: String?, value: Boolean): Boolean =
        mSharedPreferences.getBoolean(key, value)

    fun writeLong(key: String?, value: Long) {
        mSharedPreferences.edit { putLong(key, value) }
    }

    fun writeString(key: String?, value: String?) {
        mSharedPreferences.edit { putString(key, value) }
    }

    fun writeBoolean(key: String?, value: Boolean) {
        mSharedPreferences.edit { putBoolean(key, value) }
    }
}