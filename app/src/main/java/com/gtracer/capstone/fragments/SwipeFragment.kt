package com.gtracer.capstone.fragments

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import com.android.volley.Request
import com.gtracer.capstone.R
import com.gtracer.capstone.activities.GraduatesActivity
import com.gtracer.capstone.activities.ProfileActivity
import com.gtracer.capstone.activities.SettingsActivity
import com.gtracer.capstone.adapters.GraduateListAdapter
import com.gtracer.capstone.models.Graduate
import com.gtracer.capstone.models.User
import com.gtracer.capstone.objects.Companions
import com.gtracer.capstone.objects.Constant
import com.gtracer.capstone.singletons.AppVolley.Callback
import com.google.gson.Gson
import com.gtracer.capstone.databinding.FragmentSwipeBinding
import com.lorentzos.flingswipe.SwipeFlingAdapterView
import org.json.JSONObject

class SwipeFragment : Fragment(), View.OnClickListener {

    private var _binding: FragmentSwipeBinding? = null
    private var mUser: User? = null
    private var mGraduate: Graduate? = null
    private var mAdapter: GraduateListAdapter? = null
    private var mGraduateList: ArrayList<Graduate>? = null
    private lateinit var mDialog: AlertDialog
    private val mBinding get() = _binding

    companion object {
        private val TAG = SwipeFragment::class.java.simpleName
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mGraduateList = ArrayList()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentSwipeBinding.inflate(inflater, container, false)
        return mBinding?.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState); getUser()
        mAdapter = GraduateListAdapter(context, mGraduateList)
        mBinding?.flingAdapterView?.adapter = mAdapter
        mBinding?.flingAdapterView?.setFlingListener(object : SwipeFlingAdapterView.onFlingListener {
            override fun removeFirstObjectInAdapter() {
                // This is the simplest way to delete an object from the Adapter (/AdapterView)
                mGraduateList?.removeAt(0)
                mAdapter?.notifyDataSetChanged()
            }

            override fun onLeftCardExit(dataObject: Any?) {}

            override fun onRightCardExit(dataObject: Any?) {
                mGraduate = dataObject as Graduate
                val name = mGraduate?.person?.toString()
                var dialog: AlertDialog

                AlertDialog.Builder(context!!).let {
                    it.setTitle("Survey Response")
                    it.setMessage(resources.getString(R.string.text_survey_option, name))
                    it.setCancelable(false).setPositiveButton("Next", null)
                        .setNegativeButton("Later", null)
                    dialog = it.create()
                }
                dialog.show()
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener {
                    dialog.dismiss()
                    FormInputFragment().let { fragment ->
                        fragment.arguments = Bundle().also { bundle ->
                            bundle.putParcelable(Constant.GRADUATE, mGraduate)
                        }
                        fragment.setTargetFragment(
                            this@SwipeFragment, Constant.RESPONSE_REQUEST_CODE
                        )
                        fragment.show(activity!!.supportFragmentManager, TAG)
                    }
                }
                dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener {
                    HashMap<String, String>().let { body ->
                        body["graduateId"] = mGraduate?.graduateId!!
                        HashMap<String, String>().let { header ->
                            header["Content-Type"] = resources.getString(R.string.text_accept)
                            header["Authorization"] = resources.getString(
                                R.string.text_authorization,
                                Companions.getAccessToken()
                            )
                            val maps: Array<Map<String, String>> = arrayOf(body, header)
                            Companions.makeJsonRequest(
                                Request.Method.POST, Companions.save(), JSONObject(maps[0]),
                                maps[1], object : Callback {
                                    override fun onSuccess(result: JSONObject?) {
                                        Companions.toast(result?.getString("message"))
                                    }

                                    override fun onFailure(result: String?) {
                                        Companions.debugLog(TAG, result)
                                    }
                                }
                            )
                        }
                    }
                    dialog.dismiss()
                }
            }

            override fun onAdapterAboutToEmpty(itemsInAdapter: Int) {}

            override fun onScroll(scrollProgressPercent: Float) {}
        })
        setListeners()
    }

    private fun setListeners() {
        mBinding?.ibPeople?.setOnClickListener(this)
        mBinding?.ibProfile?.setOnClickListener(this)
        mBinding?.layoutButtons?.btnRepeat?.setOnClickListener(this)
        mBinding?.layoutButtons?.btnLeft?.setOnClickListener(this)
        mBinding?.layoutButtons?.btnRight?.setOnClickListener(this)
        mBinding?.layoutButtons?.btnSettings?.setOnClickListener(this)
    }

    private fun getUser() {
        Companions.dialog(context, "Processing...")
        HashMap<String, String>().let { header ->
            header["Accept"] = resources.getString(R.string.text_accept)
            header["Authorization"] =
                resources.getString(R.string.text_authorization, Companions.getAccessToken())
            Companions.makeJsonRequest(Request.Method.GET, Companions.user(), null, header,
                object : Callback {
                    override fun onSuccess(result: JSONObject?) {
                        val userJson = result?.getJSONObject(Constant.USER)
                        mUser = Gson().fromJson(userJson?.toString(), User::class.java)
                        fetchGraduates()
                    }

                    override fun onFailure(result: String?) {
                        Companions.debugLog(TAG, result)
                    }
                }
            )
        }
    }

    private fun fetchGraduates() {
        HashMap<String, String>().let { header ->
            header["Accept"] = resources.getString(R.string.text_accept)
            header["Authorization"] =
                resources.getString(R.string.text_authorization, Companions.getAccessToken())
            Companions.makeJsonRequest(Request.Method.GET, Companions.graduates(), null, header,
                object : Callback {
                    override fun onSuccess(result: JSONObject?) {
                        val jsonArray = result?.getJSONArray("graduates")

                        for (i in 0 until jsonArray!!.length()) {
                            val obj = jsonArray.getJSONObject(i).toString()
                            val graduate = Gson().fromJson(obj, Graduate::class.java)

                            if (!mGraduateList!!.contains(graduate)) {
                                mGraduateList?.add(graduate)
                            }
                        }

                        Companions.dialog.dismiss()
                        mAdapter?.notifyDataSetChanged()
                    }

                    override fun onFailure(result: String?) {
                        Companions.dialog.dismiss()
                        Companions.debugLog(TAG, result)
                    }
                }
            )
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.ibPeople -> {
                Intent(context, GraduatesActivity::class.java).also {
                    startActivity(it)
                }
            }
            R.id.ibProfile -> {
                Intent(context, ProfileActivity::class.java).also {
                    startActivity(it)
                }
            }
            R.id.btnRepeat -> {
                mGraduateList?.clear()
                mAdapter?.notifyDataSetChanged()
                getUser()
            }
            R.id.btnSettings -> {
                Intent(context, SettingsActivity::class.java).also {
                    it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                    it.putExtra(Constant.USER, mUser); startActivity(it)
                }
            }
            R.id.btnLeft -> {
                if (mGraduateList!!.isNotEmpty()) {
                    mBinding?.flingAdapterView?.topCardListener?.selectLeft()
                }
            }
            R.id.btnRight -> {
                if (mGraduateList!!.isNotEmpty()) {
                    mBinding?.flingAdapterView?.topCardListener?.selectRight()
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Constant.RESPONSE_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                AlertDialog.Builder(context!!).let { builder ->
                    builder.setMessage(data?.getStringExtra("message"))
                        .setCancelable(false).setPositiveButton("OK") { dialog, _ ->
                            dialog.dismiss()
                        }
                    mDialog = builder.create()
                    mDialog.show()
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}