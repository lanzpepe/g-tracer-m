package com.gtracer.capstone.objects

import android.app.Activity
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Context
import android.net.Uri
import android.text.TextUtils
import android.util.Log
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.AutoCompleteTextView
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentTransaction
import com.android.volley.*
import com.android.volley.toolbox.HttpHeaderParser
import com.android.volley.toolbox.JsonObjectRequest
import com.google.android.material.snackbar.Snackbar
import com.gtracer.capstone.R
import com.gtracer.capstone.databinding.DialogProgressBinding
import com.gtracer.capstone.singletons.AppInstance
import com.gtracer.capstone.singletons.AppVolley
import com.gtracer.capstone.singletons.AppVolley.Callback
import com.gtracer.capstone.utils.CacheRequest
import org.json.JSONException
import org.json.JSONObject
import java.io.UnsupportedEncodingException
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern

object Companions {
    private const val DOMAIN_NAME = "gtracer.herokuapp.com"
    // private const val DOMAIN_NAME = "192.168.1.4"
    private const val BASE_URL = "https://$DOMAIN_NAME/api/v1"
    private const val PATTERN_NAME = "^[A-z\\s-]+$"
    private val mContext = AppInstance.getContext()
    lateinit var dialog: Dialog

    // G-TRACER URL FUNCTIONS

    fun login(): String = "$BASE_URL/login"

    fun register(): String = "$BASE_URL/register"

    fun genders(): String = "$BASE_URL/list/gender"

    fun schools(): String = "$BASE_URL/list/school"

    fun courses(): String = "$BASE_URL/list/course"

    fun schoolYears(): String = "$BASE_URL/list/school_year"

    fun batches(): String = "$BASE_URL/list/batch"

    fun jobs(): String = "$BASE_URL/list/job"

    fun companies(): String = "$BASE_URL/list/company"

    fun linkedIn(): String = "$BASE_URL/register/linkedin"

    fun verify(): String = "$BASE_URL/register/verify"

    fun verification(): String = "$BASE_URL/register/verification"

    fun token(): String = "$BASE_URL/register/token"

    fun user(): String = "$BASE_URL/user"

    fun graduates(): String = "$BASE_URL/user/graduates"

    fun update(): String = "$BASE_URL/user/job/update"

    fun socialAccount(): String = "$BASE_URL/user/social/account"

    fun claimReward(): String = "$BASE_URL/user/reward/claim"

    fun preference(): String = "$BASE_URL/user/preference/set"

    fun imageUri(uri: String?): String = "http://$DOMAIN_NAME${Uri.decode(uri)}"

    fun save(): String = "$BASE_URL/graduate/save"

    fun graduate(id: String?): String = "$BASE_URL/graduates/$id"

    fun response(): String = "$BASE_URL/graduate/response"

    fun rewards(): String = "$BASE_URL/list/reward"

    fun logout(): String = "$BASE_URL/logout"

    // END

    fun getAccessToken(): String? = PrefsHelper.readString(Constant.ACCESS_TOKEN, "")

    fun getProviderId(): String? = PrefsHelper.readString(Constant.PROVIDER_ID, "")

    fun getProviderName(): String? = PrefsHelper.readString(Constant.PROVIDER_NAME, "")

    fun debugLog(tag: String?, message: String?) = Log.d(tag, message!!)

    fun toast(message: String?) =
        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show()

    fun snackbar(view: View, message: String) =
        Snackbar.make(view, message, Snackbar.LENGTH_LONG).show()

    fun isError(view: View?, input: CharSequence?, tag: String?): Boolean {
        return if (TextUtils.isEmpty(input)) {
            toast(mContext.resources?.getString(R.string.text_required, tag))
            setBackgroundError(view, true); true
        } else if ((!Pattern.compile(PATTERN_NAME).matcher(input!!).matches() &&
                    (tag.equals("First name", true) || tag.equals("Last name", true))) ||
            (!Patterns.EMAIL_ADDRESS.matcher(input).matches() && tag == "Email address")
        ) {
            setBackgroundError(view, true)
            toast(mContext.resources?.getString(R.string.text_invalid, tag)); true
        } else {
            setBackgroundError(view, false); false
        }
    }

    fun setBackgroundError(view: View?, error: Boolean) {
        if (error)
            view?.setBackgroundResource(R.drawable.button_shape_gray_error)
        else
            view?.setBackgroundResource(R.drawable.button_shape_gray)
    }

    fun hideKeyboard(activity: Activity, view: View?) =
        (activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager)
            .hideSoftInputFromWindow(view?.windowToken, 0)

    fun date(context: Context?, view: View?) {
        Calendar.getInstance().let {
            val dateFormat = SimpleDateFormat(
                mContext.resources.getString(R.string.text_standard_date), Locale.US
            )
            val datePicker = DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
                it.set(Calendar.YEAR, year)
                it.set(Calendar.MONTH, month)
                it.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                if (view is TextView)
                    view.text = dateFormat.format(it.time)
                else if (view is AutoCompleteTextView)
                    view.setText(dateFormat.format(it.time))
            }

            DatePickerDialog(
                context!!, datePicker, it.get(Calendar.YEAR),
                it.get(Calendar.MONTH), it.get(Calendar.DAY_OF_MONTH)
            ).show()
        }
    }

    fun getTime(date: String): Long = SimpleDateFormat(
        mContext.resources.getString(R.string.text_standard_date), Locale.US
    ).parse(date)!!.time

    fun dialog(context: Context?, title: String?): Dialog {
        val binding = DialogProgressBinding.inflate(LayoutInflater.from(context))

        dialog = Dialog(context!!, R.style.CustomProgressBar)
        binding.tvTitle.text = title
        dialog.setContentView(binding.root)
        dialog.show()

        return dialog
    }

    fun setStatusBarColor(context: Context, window: Window) {
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(context, R.color.colorApp)
    }

    fun slideTransition(activity: FragmentActivity?) =
        activity?.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)

    fun fadeTransition(activity: FragmentActivity?) =
        activity?.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)

    fun makeCacheRequest(m: Int, u: String, h: Map<String, String>, c: Callback) {
        val cacheRequest = object : CacheRequest(m, u, Response.Listener {
            try {
                if (it != null) {
                    val charset = charset(HttpHeaderParser.parseCharset(it.headers))
                    c.onSuccess(JSONObject(String(it.data, charset)))
                }
            } catch (e: UnsupportedEncodingException) {
                e.printStackTrace()
            }
        }, Response.ErrorListener {
            onError(c, it)
        }) {
            override fun getHeaders(): MutableMap<String, String> = h as MutableMap<String, String>
        }

        addVolleyRequest(cacheRequest)
    }

    fun makeJsonRequest(
        m: Int, u: String, b: JSONObject?, h: Map<String, String>, c: Callback
    ) {
        val jsonRequest = object : JsonObjectRequest(m, u, b, Response.Listener {
            try {
                if (it != null)
                    c.onSuccess(it)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }, Response.ErrorListener {
            onError(c, it)
        }) {
            override fun getHeaders(): MutableMap<String, String> = h as MutableMap<String, String>
        }

        addVolleyRequest(jsonRequest)
    }

    private fun addVolleyRequest(request: Request<*>) {
        request.retryPolicy = DefaultRetryPolicy(Constant.TIMEOUT_MS, 1, 1.0f)
        AppVolley.getInstance(mContext).addToRequestQueue(request)
    }

    fun addFragment(activity: FragmentActivity, otherFragment: Fragment, tag: String) =
        activity.supportFragmentManager.beginTransaction()
            .add(R.id.fragment, otherFragment, tag).commit()

    fun replaceFragment(activity: FragmentActivity, otherFragment: Fragment) =
        activity.supportFragmentManager.beginTransaction()
            .replace(R.id.fragment, otherFragment)
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).commit()

    private fun onError(callback: Callback, error: VolleyError) {
        var err: String? = null

        if (error is NoConnectionError || error is NetworkError)
            err = "No internet connection."
        else if (error is TimeoutError)
            err = "Network timeout error."
        else if (error is ServerError || error is AuthFailureError)
            err = handleServerError(error)

        try {
            if (err != null)
                callback.onFailure(err)
            else
                callback.onFailure(error.message)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun handleServerError(error: VolleyError): String? {
        val response = error.networkResponse

        if (response != null) {
            when (response.statusCode) {
                404, 401, 400 -> {
                    try {
                        val jsonString = String(error.networkResponse.data)
                        val jsonObject = JSONObject(jsonString)

                        if (jsonObject.has("message"))
                            return jsonObject.get("message").toString()
                        else if (jsonObject.has("error_description"))
                            return jsonObject.get("error_description").toString()
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                    return error.message
                }
                else -> return "Unknown status code."
            }
        }

        return "General error."
    }
}