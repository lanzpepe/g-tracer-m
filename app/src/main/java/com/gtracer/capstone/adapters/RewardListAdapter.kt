package com.gtracer.capstone.adapters

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.gtracer.capstone.R
import com.gtracer.capstone.activities.CheckoutActivity
import com.gtracer.capstone.databinding.ItemRewardBinding
import com.gtracer.capstone.fragments.ImageFragment
import com.gtracer.capstone.models.Reward
import com.gtracer.capstone.models.User
import com.gtracer.capstone.objects.Companions
import com.gtracer.capstone.utils.GlideApp

class RewardListAdapter(var context: Context?, var user: User, var rewards: ArrayList<Reward>?) :
    RecyclerView.Adapter<RewardListAdapter.CustomViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val binding = ItemRewardBinding.inflate(LayoutInflater.from(parent.context), parent, false)

        return CustomViewHolder(binding)
    }

    override fun getItemCount(): Int = rewards!!.size

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        val reward = rewards!![position]
        val uri = Companions.imageUri(reward.imageUri).replace("+", "%20")

        GlideApp.with(context!!).load(uri).into(holder.image)
        holder.name.text = reward.name
        holder.description.text = reward.description
        holder.uploader.text = reward.admins!![0].departments!![0].name
        holder.points.text = reward.points.toString()
        when (reward.admins!![0].pivot!!.quantity) {
            0 -> {
                holder.status.text = context?.resources?.getString(R.string.text_item_status_0)
                holder.claim.isEnabled = false
            }
            else -> {
                holder.status.text = context?.resources?.getString(R.string.text_item_status_1)
                holder.claim.isEnabled = true
            }
        }
        holder.image.setOnClickListener {
            ImageFragment().let { fragment ->
                fragment.arguments = Bundle().also { bundle ->
                    bundle.putStringArray("items", arrayOf(reward.name, uri))
                }
                fragment.show((context as FragmentActivity).supportFragmentManager, "image")
            }
        }
        holder.claim.setOnClickListener {
            if (user.achievement!!.points < reward.points) {
                Companions.toast("Insufficient points.")
            } else {
                Intent(context, CheckoutActivity::class.java).let { intent ->
                    intent.putExtra("user-reward", arrayOf(reward, user))
                    context!!.startActivity(intent)
                }
            }
        }
    }

    class CustomViewHolder(binding: ItemRewardBinding) : RecyclerView.ViewHolder(binding.root) {
        var image = binding.ivItemImage
        var name = binding.tvItemName
        var description = binding.tvItemDescription
        var uploader = binding.tvItemUploader
        var points = binding.tvItemPoints
        var status = binding.tvItemStatus
        var claim = binding.btnClaim
    }
}

