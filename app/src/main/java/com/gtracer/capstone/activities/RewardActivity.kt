package com.gtracer.capstone.activities

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Request
import com.gtracer.capstone.R
import com.gtracer.capstone.adapters.RewardListAdapter
import com.gtracer.capstone.models.Reward
import com.gtracer.capstone.models.User
import com.gtracer.capstone.objects.Companions
import com.gtracer.capstone.objects.Constant
import com.gtracer.capstone.singletons.AppVolley.Callback
import com.google.gson.Gson
import com.gtracer.capstone.databinding.ActivityRewardBinding
import org.json.JSONObject

class RewardActivity : AppCompatActivity() {

    private var mAdapter: RewardListAdapter? = null
    private var mRewardList: ArrayList<Reward>? = null
    private lateinit var mBinding: ActivityRewardBinding
    private lateinit var mUser: User

    companion object {
        private const val TAG = "RewardActivity"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityRewardBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        setSupportActionBar(mBinding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(true)
        mUser = intent.getParcelableExtra(Constant.USER)!!
        mRewardList = ArrayList()
        mBinding.tvPoints.text = resources?.getString(R.string.text_avail_point, mUser.achievement?.points)
        fetchRewardItems()
        mAdapter = RewardListAdapter(this, mUser, mRewardList)
        mBinding.rvRewards.setHasFixedSize(true)
        mBinding.rvRewards.adapter = mAdapter
        mBinding.btnHistory.setOnClickListener {
            Intent(this, TransactionActivity::class.java).also { intent ->
                startActivity(intent)
            }
        }
    }

    private fun fetchRewardItems() {
        HashMap<String, String>().let { header ->
            header["Accept"] = resources.getString(R.string.text_accept)
            Companions.makeJsonRequest(Request.Method.GET, Companions.rewards(), null, header,
                object : Callback {
                    override fun onSuccess(result: JSONObject?) {
                        populateItemList(result)
                    }

                    override fun onFailure(result: String?) {
                        Companions.debugLog(TAG, result)
                    }
                }
            )
        }
    }

    private fun populateItemList(result: JSONObject?) {
        val jsonArray = result?.getJSONArray("rewards")

        mRewardList?.clear()

        for (i in 0 until jsonArray!!.length()) {
            mRewardList?.add(
                Gson().fromJson(
                    jsonArray.getJSONObject(i).toString(),
                    Reward::class.java
                )
            )
        }

        mAdapter?.notifyDataSetChanged()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onResume() {
        super.onResume()
        fetchRewardItems()
    }
}