package com.gtracer.capstone.singletons

import android.app.Application
import android.content.Context
import android.text.TextUtils
import android.util.Base64
import androidx.multidex.MultiDex
import com.gtracer.capstone.models.*
import com.gtracer.capstone.objects.Companions
import com.gtracer.capstone.objects.PrefsHelper
import com.google.android.libraries.places.api.Places

class AppInstance : Application() {

    companion object {
        private var sInstance: AppInstance? = null
        private var sCompany: Company? = null
        private var sContact: Contact? = null
        private var sCourse: Course? = null
        private var sEducation: Education? = null
        private var sPerson: Person? = null
        private var sResponse: Response? = null
        private var sUser: User? = null

        init {
            System.loadLibrary("keys")
        }

        fun getContext() = sInstance ?: AppInstance().also {
            sInstance = it
        }

        fun getCompany() = sCompany ?: Company().also {
            sCompany = it
        }

        fun getContact() = sContact ?: Contact().also {
            sContact = it
        }

        fun getCourse() = sCourse ?: Course().also {
            sCourse = it
        }

        fun getEducation() = sEducation ?: Education().also {
            sEducation = it
        }

        fun getPerson() = sPerson ?: Person().also {
            sPerson = it
        }

        fun getResponse() = sResponse ?: Response().also {
            sResponse = it
        }

        fun getUser() = sUser ?: User().also {
            sUser = it
        }

        external fun getGoogleMapsApiKey(): String?

        external fun getLinkedInClientKey(): String?

        external fun getLinkedInSecretKey(): String?
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    override fun onCreate() {
        super.onCreate(); sInstance = this
        initPlaces(); PrefsHelper.init(this)
    }

    private fun initPlaces() {
        val key = String(Base64.decode(getGoogleMapsApiKey(), Base64.DEFAULT))

        if (TextUtils.isEmpty(key)) {
            Companions.toast("API key is missing.")
            return
        }

        if (!Places.isInitialized())
            Places.initialize(this, key)
    }
}