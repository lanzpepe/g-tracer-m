package com.gtracer.capstone.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class UserReward(
    var id: String? = null,
    @SerializedName("user_id") var userId: String? = null,
    @SerializedName("reward_id") var rewardId: String? = null,
    var status: Int = 0, @SerializedName("created_at") var createdAt: String? = null,
    @SerializedName("updated_at") var updatedAt: String? = null
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readInt(),
        parcel.readString(),
        parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(userId)
        parcel.writeString(rewardId)
        parcel.writeInt(status)
        parcel.writeString(createdAt)
        parcel.writeString(updatedAt)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<UserReward> {
        override fun createFromParcel(parcel: Parcel): UserReward {
            return UserReward(parcel)
        }

        override fun newArray(size: Int): Array<UserReward?> {
            return arrayOfNulls(size)
        }
    }
}