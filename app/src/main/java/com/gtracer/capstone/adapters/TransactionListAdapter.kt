package com.gtracer.capstone.adapters

import android.content.Context
import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.gtracer.capstone.R
import com.gtracer.capstone.databinding.ItemHistoryBinding
import com.gtracer.capstone.models.Reward
import java.text.SimpleDateFormat
import java.util.*

class TransactionListAdapter(var context: Context?, var rewards: ArrayList<Reward>?) :
    RecyclerView.Adapter<TransactionListAdapter.CustomViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val binding = ItemHistoryBinding.inflate(LayoutInflater.from(parent.context), parent, false)

        return CustomViewHolder(binding)
    }

    override fun getItemCount(): Int = rewards!!.size

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        val reward = rewards!![position]
        val status = if (reward.pivot!!.status == 0) "checked out" else "claimed"
        val time = SimpleDateFormat(
            context?.resources?.getString(R.string.text_iso_8601_date), Locale.US)
            .parse(reward.pivot?.createdAt!!)!!.time

        holder.message.text = context?.resources?.getString(
            R.string.text_confirmation_message, status, reward.name, reward.points
        )
        holder.id.text = context?.resources?.getString(
            R.string.text_confirmation_id, reward.pivot?.id
        )
        holder.timestamp.text = DateUtils.getRelativeTimeSpanString(time)
    }

    class CustomViewHolder(binding: ItemHistoryBinding) : RecyclerView.ViewHolder(binding.root) {
        var id = binding.tvConfirmationNumber
        var message = binding.tvConfirmationMessage
        var timestamp = binding.tvConfirmationTimestamp
    }
}