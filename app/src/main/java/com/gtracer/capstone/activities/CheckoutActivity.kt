package com.gtracer.capstone.activities

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Request
import com.gtracer.capstone.R
import com.gtracer.capstone.databinding.ActivityCheckoutBinding
import com.gtracer.capstone.models.Reward
import com.gtracer.capstone.models.User
import com.gtracer.capstone.objects.Companions
import com.gtracer.capstone.singletons.AppVolley.Callback
import com.gtracer.capstone.utils.GlideApp
import org.json.JSONObject

class CheckoutActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var mReward: Reward
    private lateinit var mDialog: AlertDialog
    private lateinit var mBinding: ActivityCheckoutBinding

    companion object {
        private val TAG = CheckoutActivity::class.java.simpleName
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityCheckoutBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        setSupportActionBar(mBinding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(true)
        mReward = intent.getParcelableArrayExtra("user-reward")!![0] as Reward
        val user = intent.getParcelableArrayExtra("user-reward")!![1] as User
        val uri = Companions.imageUri(mReward.imageUri).replace("+", "%20")

        GlideApp.with(this).load(uri).into(mBinding.ivRewardItem)
        mBinding.tvRewardName.text = mReward.name
        mBinding.tvRewardPoints.text = resources.getString(R.string.text_points, mReward.points)
        mBinding.tvPoints.text = user.achievement?.points?.toString()
        mBinding.tvDeductPoints.text = mReward.points.toString()
        mBinding.tvRemainPoints.text = (user.achievement?.points?.minus(mReward.points))?.toString()
        mBinding.btnExchange.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.btnExchange -> {
                AlertDialog.Builder(this).let { builder ->
                    builder.setTitle("Exchange Points")
                        .setMessage("Confirm transaction? This cannot be undone.")
                        .setCancelable(false)
                        .setPositiveButton("OK", null)
                        .setNegativeButton("Cancel", null)
                    mDialog = builder.create(); mDialog.show()
                    mDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener {
                        HashMap<String, String>().let { header ->
                            header["Content-Type"] = resources.getString(R.string.text_accept)
                            header["Authorization"] = resources.getString(
                                R.string.text_authorization, Companions.getAccessToken()
                            )
                            HashMap<String, String>().let { body ->
                                body["rewardId"] = mReward.id!!
                                body["rewardPoints"] = mReward.points.toString()
                                Companions.dialog(this, "Processing...")
                                sendRequestToServer(arrayOf(header, body))
                            }
                        }
                        mDialog.dismiss()
                    }
                }
            }
        }
    }

    private fun sendRequestToServer(params: Array<Map<String, String>>) {
        Companions.makeJsonRequest(Request.Method.POST, Companions.claimReward(),
            JSONObject(params[1]), params[0], object : Callback {
                override fun onSuccess(result: JSONObject?) {
                    AlertDialog.Builder(this@CheckoutActivity).let { builder ->
                        builder.setTitle("Confirmation Message")
                            .setMessage(result?.getString("message"))
                            .setCancelable(false)
                            .setPositiveButton("OK") { dialog, _ ->
                                startActivity(); dialog.dismiss()
                            }
                        mDialog = builder.create()
                        mDialog.show()
                    }
                    Companions.dialog.dismiss()
                }

                override fun onFailure(result: String?) {
                    AlertDialog.Builder(this@CheckoutActivity).let { builder ->
                        builder.setTitle("Confirmation Message")
                            .setMessage(result)
                            .setCancelable(false)
                            .setPositiveButton("OK") { dialog, _ ->
                                dialog.dismiss()
                            }
                        mDialog = builder.create()
                        mDialog.show()
                    }
                    Companions.dialog.dismiss()
                }
            }
        )
    }

    private fun startActivity() {
        Intent(this, ProfileActivity::class.java).also {
            it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(it); finish()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }

        return super.onOptionsItemSelected(item)
    }
}