package com.gtracer.capstone.adapters

import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.core.content.ContextCompat
import com.gtracer.capstone.R
import com.gtracer.capstone.databinding.ItemCardBinding
import com.gtracer.capstone.models.Graduate
import com.gtracer.capstone.objects.Companions
import com.gtracer.capstone.utils.GlideApp

class GraduateListAdapter(context: Context?, graduates: ArrayList<Graduate>?) :
    ArrayAdapter<Graduate>(context!!, 0, graduates!!) {

    private lateinit var mBinding: ItemCardBinding

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var url: String? = null; val view: View
        val graduate = getItem(position)

        if (convertView == null) {
            mBinding = ItemCardBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            view = mBinding.root

            val uri = ContextCompat.getDrawable(parent.context, setDefaultImage(graduate))
            val fullName = graduate?.person?.toString()
            if (graduate?.imageUri != null) {
                url = Uri.decode(graduate.imageUri).replace("+", "%20")
            }
            // val result = ImageTask().execute(url)
            GlideApp.with(parent.context).load(Companions.imageUri(url)).placeholder(uri)
                .into(mBinding.ivGraduateImage)
            mBinding.tvGraduateName.text = fullName
            mBinding.tvGraduateDegree.text = graduate?.academic?.course?.title
            if (graduate?.academic?.course?.major!!.equals("None", true))
                mBinding.tvGraduateMajor.visibility = View.GONE
            else
                mBinding.tvGraduateMajor.text = parent.context.resources.getString(
                    R.string.text_major_profile,
                    graduate.academic?.course?.major
                )
            mBinding.tvGraduateSchool.text = graduate.academic?.school
            mBinding.tvGraduateDateGraduated.text = parent.context.resources.getString(
                R.string.text_date_graduated, graduate.academic?.month,
                graduate.academic?.day, graduate.academic?.year
            )
        } else
            view = convertView

        return view
    }

    private fun setDefaultImage(graduate: Graduate?): Int {
        return if (graduate?.person?.gender == "Male") {
            R.drawable.default_avatar_m
        } else {
            R.drawable.default_avatar_m
        }
    }
}
