package com.gtracer.capstone.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.gtracer.capstone.R
import com.gtracer.capstone.databinding.ItemRowBinding
import com.gtracer.capstone.models.Graduate
import com.gtracer.capstone.objects.Companions
import com.gtracer.capstone.threads.ImageTask

class SavedGraduatesAdapter(var mContext: Context?, var mGraduateList: ArrayList<Graduate>?) :
    RecyclerView.Adapter<SavedGraduatesAdapter.CustomViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val binding = ItemRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)

        return CustomViewHolder(binding)
    }

    override fun getItemCount(): Int = mGraduateList!!.size

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        val graduate = mGraduateList!![position]
        val fullName = graduate.person?.toString()

        val uri = if (graduate.person?.gender == "Male")
            ContextCompat.getDrawable(mContext!!, R.drawable.default_avatar_m)
        else
            ContextCompat.getDrawable(mContext!!, R.drawable.default_avatar_f)
        val url = Companions.imageUri(graduate.imageUri).replace("+", "%20")
        ImageTask(holder.image, url, uri!!).execute(url)
        holder.name.text = fullName
        holder.school.text = graduate.academic?.school
        if (graduate.respondent?.responseId == null) {
            holder.status.text = mContext?.resources?.getString(R.string.text_status_0)
            holder.status.setTextColor(ContextCompat.getColor(mContext!!, R.color.quantum_googgreen800))
        } else {
            holder.status.text = mContext?.resources?.getString(R.string.text_status_1)
            holder.status.setTextColor(ContextCompat.getColor(mContext!!, R.color.quantum_error_light))
        }
    }

    class CustomViewHolder(binding: ItemRowBinding) : RecyclerView.ViewHolder(binding.root) {
        val image = binding.ivGraduateImage
        val name = binding.tvGraduateName
        val school = binding.tvGraduateSchool
        val status = binding.tvResponseStatus
    }
}