package com.gtracer.capstone.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class AdminReward(
    @SerializedName("reward_id") var rewardId: String?,
    @SerializedName("admin_id") var adminId: String?,
    var quantity: Int
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readInt()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(rewardId)
        parcel.writeString(adminId)
        parcel.writeInt(quantity)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<AdminReward> {
        override fun createFromParcel(parcel: Parcel): AdminReward {
            return AdminReward(parcel)
        }

        override fun newArray(size: Int): Array<AdminReward?> {
            return arrayOfNulls(size)
        }
    }
}