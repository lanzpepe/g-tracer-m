package com.gtracer.capstone.adapters

import android.annotation.TargetApi
import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import com.gtracer.capstone.databinding.ItemSliderBinding

@TargetApi(Build.VERSION_CODES.P)
class SliderAdapter(private val context: Context?, private val gifs: Array<Int>?) : PagerAdapter() {

    override fun isViewFromObject(view: View, `object`: Any): Boolean = view == `object`

    override fun getCount(): Int = gifs!!.size

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val binding = ItemSliderBinding.inflate(LayoutInflater.from(context), container, false)
        val uri = ContextCompat.getDrawable(context!!, gifs!![position])
        val titles = arrayOf(
            "Dismiss Graduate you don't know", "Tag Graduate you may know",
            "Survey a Graduate to earn points", "Exchange points with amazing reward"
        )
        val descriptions = arrayOf(
            "Dismiss a graduate by swiping it to the left.",
            "Tag a graduate by swiping it to the right.",
            "Complete the survey to earn points.",
            "Accumulate the points and exchange with amazing reward."
        )

        binding.tvTitle.text = titles[position]
        Glide.with(context).load(gifs[position]).placeholder(uri).into(binding.ivGifImage)
        binding.tvDescription.text = descriptions[position]
        container.addView(binding.root, 0)

        return binding.root
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }
}