package com.gtracer.capstone.models

import android.os.Parcel
import android.os.Parcelable

data class Course(
    var id: String? = null, var code: String? = null, var title: String? = null,
    var major: String? = null, var departments: ArrayList<Department>? = null
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.createTypedArrayList(Department.CREATOR)
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(code)
        parcel.writeString(title)
        parcel.writeString(major)
        parcel.writeTypedList(departments)
    }

    override fun describeContents(): Int = 0

    override fun toString(): String = "$code - $title"

    companion object CREATOR : Parcelable.Creator<Course> {
        override fun createFromParcel(parcel: Parcel): Course {
            return Course(parcel)
        }

        override fun newArray(size: Int): Array<Course?> {
            return arrayOfNulls(size)
        }
    }
}