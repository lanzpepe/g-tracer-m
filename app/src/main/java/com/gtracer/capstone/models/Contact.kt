package com.gtracer.capstone.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class Contact(
    @SerializedName("contact_id") var contactId: String? = null,
    var address: String? = null, var city: String? = null, var province: String? = null,
    @SerializedName("contact_number") var contactNumber: String? = null,
    var email: String? = null
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(contactId)
        parcel.writeString(address)
        parcel.writeString(city)
        parcel.writeString(province)
        parcel.writeString(contactNumber)
        parcel.writeString(email)
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun toString(): String {
        return "$address, $city, $province"
    }

    companion object CREATOR : Parcelable.Creator<Contact> {
        override fun createFromParcel(parcel: Parcel): Contact = Contact(parcel)

        override fun newArray(size: Int): Array<Contact?> = arrayOfNulls(size)
    }
}