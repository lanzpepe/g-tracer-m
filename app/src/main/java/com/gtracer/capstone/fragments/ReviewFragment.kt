package com.gtracer.capstone.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ScrollView
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import com.android.volley.Request
import com.gtracer.capstone.MainActivity
import com.gtracer.capstone.R
import com.gtracer.capstone.activities.RegisterActivity
import com.gtracer.capstone.databinding.FragmentReviewBinding
import com.gtracer.capstone.models.*
import com.gtracer.capstone.objects.Companions
import com.gtracer.capstone.objects.Constant
import com.gtracer.capstone.objects.PrefsHelper
import com.gtracer.capstone.singletons.AppInstance
import com.gtracer.capstone.singletons.AppVolley.Callback
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class ReviewFragment : Fragment(), View.OnClickListener, Callback {

    private var _binding: FragmentReviewBinding? = null
    private var mCompany: Company? = null
    private var mContact: Contact? = null
    private var mCourse: Course? = null
    private var mEducation: Education? = null
    private var mPerson: Person? = null
    private var mResponse: Response? = null
    private var mUser: User? = null
    private var mSimpleDateFormat: SimpleDateFormat? = null
    private lateinit var mDialog: AlertDialog
    private val mBinding get() = _binding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentReviewBinding.inflate(inflater, container, false)
        return mBinding?.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val format = resources.getString(R.string.text_full_date)
        mSimpleDateFormat = SimpleDateFormat(format, Locale.US)
        initInstances(); setValues(); setListeners()
        mBinding?.nav?.btnDone?.visibility = View.VISIBLE
        mBinding?.nav?.btnNext?.visibility = View.GONE
    }

    private fun initInstances() {
        mCompany = AppInstance.getCompany()
        mContact = AppInstance.getContact()
        mCourse = AppInstance.getCourse()
        mEducation = AppInstance.getEducation()
        mPerson = AppInstance.getPerson()
        mResponse = AppInstance.getResponse()
        mUser = AppInstance.getUser()
    }

    private fun setValues() {
        mBinding?.tvFirstName?.text = mPerson?.firstName
        mBinding?.tvMidName?.text = mPerson?.middleName?.substring(0, 1)
        mBinding?.tvLastName?.text = mPerson?.lastName
        mBinding?.tvBirthDate?.text = mSimpleDateFormat?.format(Date(getTime(mPerson?.birthDate!!)))
        mBinding?.tvGender?.text = mPerson?.gender
        mBinding?.tvAddress?.text = mContact?.address
        mBinding?.tvCity?.text = mContact?.city
        mBinding?.tvProvince?.text = mContact?.province
        mBinding?.tvContactNumber?.text = mContact?.contactNumber
        mBinding?.tvEmail?.text = mContact?.email
        mBinding?.tvDegree?.text = mCourse?.title
        if (mCourse?.major!!.isNotEmpty())
            mBinding?.tvMajor?.text = mCourse?.major
        else
            mBinding?.linear10?.visibility = View.GONE
        mBinding?.tvSchool?.text = mEducation?.school
        mBinding?.tvDateGraduated?.text = resources.getString(
            R.string.text_batch_year, mEducation?.month, mEducation?.year
        )
        mBinding?.tvCompanyName?.text = mCompany?.name
        mBinding?.tvCompanyAddress?.text = mCompany?.address
        mBinding?.tvPosition?.text = mResponse?.jobPosition
        mBinding?.tvDateHired?.text =
            mSimpleDateFormat?.format(Date(getTime(mResponse?.dateEmployed!!)))
    }

    private fun setListeners() {
        mBinding?.nav?.btnPrev?.setOnClickListener(this)
        mBinding?.nav?.btnDone?.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.btnPrev -> replaceFragment(EmploymentFragment())
            R.id.btnDone -> submitUserInformation()
        }
        (activity as RegisterActivity).getScrollView().fullScroll(ScrollView.FOCUS_UP)
    }

    private fun submitUserInformation() {
        PrefsHelper.writeBoolean(Constant.TUTORIAL, false)
        AlertDialog.Builder(context!!).let { builder ->
            builder.setTitle("Submit Information")
                .setMessage(resources.getString(R.string.text_certify))
                .setCancelable(false)
                .setPositiveButton("OK", null)
                .setNegativeButton("Cancel", null)
            mDialog = builder.create(); mDialog.show()
            mDialog.getButton(AlertDialog.BUTTON_POSITIVE)?.setOnClickListener {
                Companions.dialog(context, "Processing...")
                HashMap<String, String>().let { header ->
                    header["Content-Type"] = resources.getString(R.string.text_accept)
                    store(header, setBody())
                }
                mDialog.dismiss()
            }
        }
    }

    private fun setBody(): HashMap<String, String> {
        return HashMap<String, String>().also { body ->
            body["provider_id"] = Companions.getProviderId()!!
            body["provider_name"] = Companions.getProviderName()!!
            body["lastName"] = mPerson?.lastName!!
            body["firstName"] = mPerson?.firstName!!
            body["middleName"] = mPerson?.middleName?.substring(0, 1)!!
            body["birthDate"] = mSimpleDateFormat?.format(Date(getTime(mPerson?.birthDate!!)))!!
            body["gender"] = mPerson?.gender!!
            body["imageUri"] = mUser?.imageUri!!
            body["address"] = context?.resources?.getString(
                R.string.text_complete_address,
                mContact?.address, mContact?.city, mContact?.province
            )!!
            body["contactNumber"] = mContact?.contactNumber!!
            body["email"] = mContact?.email!!
            body["degree"] = mCourse?.title!!
            body["major"] = mCourse?.major!!
            body["department"] = mEducation?.department!!
            body["school"] = mEducation?.school!!
            body["schoolYear"] = mEducation?.year!!
            body["batch"] = mEducation?.month!!
            body["companyName"] = mCompany?.name!!
            body["companyAddress"] = mCompany?.address!!
            body["jobPosition"] = mResponse?.jobPosition!!
            body["dateEmployed"] =
                mSimpleDateFormat?.format(Date(getTime(mResponse?.dateEmployed!!)))!!
            body["remarks"] = mResponse?.remarks!!
        }
    }

    private fun store(vararg maps: Map<String, String>) {
        Companions.makeJsonRequest(
            Request.Method.POST, Companions.register(), JSONObject(maps[1]), maps[0], this
        )
    }

    override fun onSuccess(result: JSONObject?) {
        val accessToken = result?.getString(Constant.ACCESS_TOKEN)
        val refreshToken = result?.getString(Constant.REFRESH_TOKEN)
        val expiresIn = result?.getLong(Constant.EXPIRES_IN)

        PrefsHelper.writeString(Constant.ACCESS_TOKEN, accessToken)
        PrefsHelper.writeString(Constant.REFRESH_TOKEN, refreshToken)
        PrefsHelper.writeLong(Constant.EXPIRES_IN, expiresIn!!)

        Companions.dialog.dismiss()

        AlertDialog.Builder(context!!).let { builder ->
            builder.setTitle("Confirmation")
                .setMessage("Registration successful.")
                .setCancelable(false)
                .setPositiveButton("OK") { dialog, _ ->
                    Intent(context, MainActivity::class.java).also {
                        startActivity(it); activity?.finish()
                        Companions.slideTransition(activity)
                    }
                    dialog.dismiss()
                }
            mDialog = builder.create(); mDialog.show()
        }
    }

    override fun onFailure(result: String?) {
        Companions.dialog.dismiss()
        AlertDialog.Builder(context!!).let { builder ->
            builder.setTitle("Message")
                .setMessage(result)
                .setCancelable(false)
                .setPositiveButton("OK") { dialog, _ ->
                    Intent(context, MainActivity::class.java).also {
                        startActivity(it); activity?.finish()
                        Companions.slideTransition(activity)
                    }
                    dialog.dismiss()
                }
            mDialog = builder.create(); mDialog.show()
        }
    }

    private fun replaceFragment(otherFragment: Fragment) =
        Companions.replaceFragment(activity!!, otherFragment)

    private fun getTime(date: String): Long {
        return Companions.getTime(date)
    }
}
