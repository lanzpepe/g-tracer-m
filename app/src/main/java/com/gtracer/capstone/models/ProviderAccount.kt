package com.gtracer.capstone.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class ProviderAccount(
    @SerializedName("provider_id") var providerId: String? = null,
    @SerializedName("provider_name") var providerName: String? = null,
    @SerializedName("profile_id") var username: String? = null,
    @SerializedName("user_id") var userId: String? = null
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(providerId)
        parcel.writeString(providerName)
        parcel.writeString(username)
        parcel.writeString(userId)
    }

    override fun describeContents(): Int = 0

    companion object CREATOR : Parcelable.Creator<ProviderAccount> {
        override fun createFromParcel(parcel: Parcel): ProviderAccount = ProviderAccount(parcel)

        override fun newArray(size: Int): Array<ProviderAccount?> = arrayOfNulls(size)
    }
}