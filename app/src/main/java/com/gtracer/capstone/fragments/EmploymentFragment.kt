package com.gtracer.capstone.fragments

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.ScrollView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import com.android.volley.Request
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.gtracer.capstone.R
import com.gtracer.capstone.activities.RegisterActivity
import com.gtracer.capstone.databinding.FragmentEmploymentBinding
import com.gtracer.capstone.models.Company
import com.gtracer.capstone.models.Course
import com.gtracer.capstone.models.Person
import com.gtracer.capstone.models.Response
import com.gtracer.capstone.objects.Companions
import com.gtracer.capstone.objects.Constant
import com.gtracer.capstone.objects.PrefsHelper
import com.gtracer.capstone.singletons.AppInstance
import com.gtracer.capstone.singletons.AppVolley.Callback
import org.json.JSONObject

class EmploymentFragment : Fragment(), View.OnClickListener, View.OnFocusChangeListener,
    View.OnTouchListener {

    private var _binding: FragmentEmploymentBinding? = null
    private var mCompany: Company? = null
    private var mCourse: Course? = null
    private var mResponse: Response? = null
    private var mPerson: Person? = null
    private var mJobAdapter: ArrayAdapter<String>? = null
    private lateinit var mDialog: AlertDialog
    private val mBinding get() = _binding

    companion object {
        private val TAG = EmploymentFragment::class.java.simpleName
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentEmploymentBinding.inflate(inflater, container, false)
        return mBinding?.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initInstances(); getJobs()
        HashMap<String, String>().let { header ->
            header["Content-Type"] = resources.getString(R.string.text_accept)
            HashMap<String, String>().let { body ->
                body["firstName"] = mPerson?.firstName!!
                body["lastName"] = mPerson?.lastName!!
                getLinkedInProfile(header, body)
            }
        }
        setListeners()
    }

    private fun initInstances() {
        mCompany = AppInstance.getCompany()
        mCourse = AppInstance.getCourse()
        mPerson = AppInstance.getPerson()
        mResponse = AppInstance.getResponse()
    }

    private fun getLinkedInProfile(vararg maps: Map<String, String>) {
        Companions.dialog(context, "Processing...")
        Companions.makeJsonRequest(Request.Method.POST, Companions.linkedIn(),
            JSONObject(maps[1]), maps[0], object : Callback {
                override fun onSuccess(result: JSONObject?) {
                    Companions.dialog.dismiss()
                    val profile = result?.getJSONObject("profile")
                    AlertDialog.Builder(context!!).let { builder ->
                        builder.setTitle("Your data from LinkedIn")
                            .setMessage(
                                context!!.resources.getString(
                                    R.string.text_linkedin_data,
                                    profile?.getString("company"),
                                    profile?.getString("position")
                                )
                            )
                            .setCancelable(false)
                            .setPositiveButton("Accept") { dialog, _ ->
                                dialog.dismiss()
                                PrefsHelper.writeString(
                                    Constant.PROVIDER_URL,
                                    profile?.getString("profile_url")
                                )
                                mCompany?.name = profile?.getString("company")
                                mResponse?.jobPosition = profile?.getString("position")
                                mBinding?.atvRegCompanyName?.setText(mCompany?.name)
                                mBinding?.atvRegPosition?.setText(mResponse?.jobPosition)
                            }
                        mDialog = builder.create()
                        mDialog.show()
                    }
                }

                override fun onFailure(result: String?) {
                    Companions.dialog.dismiss(); Companions.debugLog(TAG, result)
                }
            }
        )
    }

    private fun getJobs() {
        HashMap<String, String>().let { header ->
            header["Accept"] = resources.getString(R.string.text_accept)
            Companions.makeJsonRequest(Request.Method.GET, Companions.jobs(), null,
                header, object : Callback {
                    override fun onSuccess(result: JSONObject?) {
                        val jsonArray = result?.getJSONArray("jobs")
                        val jobList = ArrayList<String>()

                        for (i in 0 until jsonArray!!.length()) {
                            jobList.add(jsonArray.getJSONObject(i).getString("job_positions"))
                        }

                        mJobAdapter = ArrayAdapter(
                            context!!, android.R.layout.simple_spinner_dropdown_item, jobList
                        )
                        mBinding?.atvRegPosition?.threshold = 1
                        mBinding?.atvRegPosition?.setAdapter(mJobAdapter)
                    }

                    override fun onFailure(result: String?) {
                        Companions.debugLog(TAG, result)
                    }
                }
            )
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun setListeners() {
        mBinding?.nav?.btnNext?.setOnClickListener(this)
        mBinding?.nav?.btnPrev?.setOnClickListener(this)
        mBinding?.atvRegCompanyAddress?.onFocusChangeListener = this
        mBinding?.atvRegPosition?.onFocusChangeListener = this
        mBinding?.tvRegDateHired?.onFocusChangeListener = this
        mBinding?.layoutEmployment?.setOnTouchListener(this)
        mBinding?.atvRegCompanyAddress?.setOnTouchListener(this)
        mBinding?.tvRegDateHired?.setOnTouchListener(this)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.btnPrev -> replaceFragment(EducationFragment())
            R.id.btnNext -> saveEmployment()
        }
        (activity as RegisterActivity).getScrollView().fullScroll(ScrollView.FOCUS_UP)
    }

    override fun onFocusChange(v: View?, hasFocus: Boolean) {
        when (v!!.id) {
            R.id.atvRegCompanyAddress ->
                Companions.setBackgroundError(v, false)
            R.id.atvRegPosition ->
                Companions.setBackgroundError(v, false)
            R.id.tvRegDateHired ->
                Companions.setBackgroundError(v, false)
        }
    }

    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
        v?.performClick()

        if (event?.action == MotionEvent.ACTION_DOWN) {
            when (v!!.id) {
                R.id.layoutEmployment -> Companions.hideKeyboard(activity!!, v)
                R.id.atvRegCompanyAddress -> {
                    if (event.x >= v.right.minus((v as AutoCompleteTextView).totalPaddingEnd)) {
                        val intent = Autocomplete.IntentBuilder(
                            AutocompleteActivityMode.OVERLAY,
                            arrayListOf(Place.Field.ADDRESS)
                        ).build(context!!)
                        startActivityForResult(intent, Constant.AUTOCOMPLETE_REQUEST_CODE)
                    } else
                        v.requestFocus()
                }
                R.id.tvRegDateHired -> {
                    if (event.x >= v.right.minus((v as TextView).totalPaddingEnd))
                        Companions.date(context, mBinding?.tvRegDateHired)
                }
            }
        }

        return false
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == Constant.AUTOCOMPLETE_REQUEST_CODE) {
            when (resultCode) {
                AutocompleteActivity.RESULT_OK -> {
                    val place = Autocomplete.getPlaceFromIntent(data!!)
                    mBinding?.atvRegCompanyAddress?.setText(place.address)
                }
                AutocompleteActivity.RESULT_ERROR -> {
                    val status = Autocomplete.getStatusFromIntent(data!!)
                    Companions.toast(status.statusMessage)
                }
            }
        }

        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun saveEmployment() {
        val companyName = mBinding?.atvRegCompanyName?.text.toString().trim()
        val companyAddress = mBinding?.atvRegCompanyAddress?.text.toString().trim()
        val position = mBinding?.atvRegPosition?.text.toString().trim()
        val dateHired = mBinding?.tvRegDateHired?.text.toString().trim()

        mCompany?.name = companyName; mCompany?.address = companyAddress
        mResponse?.jobPosition = position; mResponse?.dateEmployed = dateHired
        mResponse?.remarks = "Respondent"

        if (!isError(mBinding?.atvRegCompanyName, companyName, "Company name") &&
            !isError(mBinding?.atvRegCompanyAddress, companyAddress, "Company address") &&
            !isError(mBinding?.atvRegPosition, position, "Position") &&
            !isError(mBinding?.tvRegDateHired, dateHired, "Date employed")
        ) {
            replaceFragment(ReviewFragment())
        }
    }

    private fun isError(view: View?, input: String?, tag: String?) =
        Companions.isError(view, input, tag)

    private fun replaceFragment(otherFragment: Fragment) =
        Companions.replaceFragment(activity!!, otherFragment)
}
