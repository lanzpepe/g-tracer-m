package com.gtracer.capstone.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class Employment(
    @SerializedName("user_id") var userId: String? = null,
    @SerializedName("job_position") var jobPosition: String? = null,
    @SerializedName("date_employed") var dateEmployed: String? = null,
    var company: Company? = null
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readParcelable(Company::class.java.classLoader)
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(userId)
        parcel.writeString(jobPosition)
        parcel.writeString(dateEmployed)
        parcel.writeParcelable(company, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Employment> {
        override fun createFromParcel(parcel: Parcel): Employment {
            return Employment(parcel)
        }

        override fun newArray(size: Int): Array<Employment?> {
            return arrayOfNulls(size)
        }
    }
}