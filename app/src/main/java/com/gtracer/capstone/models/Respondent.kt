package com.gtracer.capstone.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class Respondent(
    @SerializedName("graduate_id") var graduateId: String? = null,
    @SerializedName("response_id") var responseId: String? = null,
    @SerializedName("user_id") var userId: String? = null
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(graduateId)
        parcel.writeString(responseId)
        parcel.writeString(userId)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Respondent> {
        override fun createFromParcel(parcel: Parcel): Respondent {
            return Respondent(parcel)
        }

        override fun newArray(size: Int): Array<Respondent?> {
            return arrayOfNulls(size)
        }
    }
}