package com.gtracer.capstone

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Request
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessaging
import com.gtracer.capstone.activities.LoginActivity
import com.gtracer.capstone.databinding.ActivityMainBinding
import com.gtracer.capstone.databinding.ActivitySplashBinding
import com.gtracer.capstone.fragments.InstructionFragment
import com.gtracer.capstone.fragments.SwipeFragment
import com.gtracer.capstone.objects.Companions
import com.gtracer.capstone.objects.Constant
import com.gtracer.capstone.objects.PrefsHelper
import com.gtracer.capstone.singletons.AppVolley.Callback
import org.json.JSONObject

class MainActivity : AppCompatActivity(), InstructionFragment.OnFragmentDialogListener {

    private var mHandler: Handler? = null
    private lateinit var mRunnable: Runnable
    private lateinit var mBinding: ActivityMainBinding

    companion object {
        private val TAG = MainActivity::class.java.simpleName
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivitySplashBinding.inflate(layoutInflater)
        mBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        mHandler = Handler(); mRunnable = Runnable { load() }
        mHandler?.postDelayed(mRunnable, Constant.TIMEOUT_MS.toLong())
    }

    private fun load() {
        HashMap<String, String>().let { header ->
            header["Accept"] = resources.getString(R.string.text_accept)
            header["Authorization"] =
                resources.getString(R.string.text_authorization, Companions.getAccessToken())
            Companions.makeJsonRequest(Request.Method.GET, Companions.user(), null, header,
                object : Callback {
                    override fun onSuccess(result: JSONObject?) {
                        setContentView(mBinding.root)
                        val user = result?.getJSONObject(Constant.USER)
                        val tutorial = PrefsHelper.readBoolean(Constant.TUTORIAL, false)

                        if (!tutorial) {
                            InstructionFragment().show(supportFragmentManager, "dialog")
                        } else {
                            addFragment()
                        }

                        getFCMToken(user?.getString("user_id"))
                    }

                    override fun onFailure(result: String?) {
                        startActivity(Intent(this@MainActivity, LoginActivity::class.java))
                        finish(); Companions.debugLog(TAG, result)
                        Companions.fadeTransition(this@MainActivity)
                    }
                }
            )
        }
    }

    private fun getFCMToken(userId: String?) {
        FirebaseMessaging.getInstance().token.addOnCompleteListener {
            if (!it.isSuccessful) {
                Companions.debugLog(TAG, "Fetching FCM registration token failed: ${it.exception}")
                return@addOnCompleteListener
            }

            HashMap<String, String>().let { header ->
                header["Content-Type"] = resources.getString(R.string.text_accept)
                HashMap<String, String>().let { body ->
                    body["userId"] = userId!!
                    body["token"] = it.result
                    registerToken(header, body)
                }
            }
        }
    }

    private fun registerToken(vararg maps: Map<String, String>) {
        Companions.makeJsonRequest(Request.Method.POST, Companions.token(), JSONObject(maps[1]),
            maps[0], object : Callback {
                override fun onSuccess(result: JSONObject?) {
                    Companions.debugLog(TAG, result?.getString("message"))
                }

                override fun onFailure(result: String?) {
                    Companions.debugLog(TAG, result)
                }
            }
        )
    }

    private fun addFragment() {
        SwipeFragment().let { fragment ->
            Companions.addFragment(this, fragment, "swipe")
        }
    }

    override fun onDismiss() {
        addFragment()
    }

    override fun onStop() {
        super.onStop()
        mHandler?.removeCallbacks(mRunnable)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        Companions.debugLog(TAG, "onSaveInstanceState called!")
    }
}