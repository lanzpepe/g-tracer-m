package com.gtracer.capstone.activities

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.android.volley.Request
import com.bumptech.glide.Glide
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.OAuthProvider
import com.google.gson.Gson
import com.gtracer.capstone.R
import com.gtracer.capstone.databinding.ActivityProfileBinding
import com.gtracer.capstone.fragments.FormInputFragment
import com.gtracer.capstone.models.Twitter
import com.gtracer.capstone.models.User
import com.gtracer.capstone.objects.Companions
import com.gtracer.capstone.objects.Constant
import com.gtracer.capstone.singletons.AppVolley.Callback
import org.json.JSONObject

class ProfileActivity : AppCompatActivity(), View.OnTouchListener,
    FormInputFragment.OnDialogFragmentListener {

    private var mAuth: FirebaseAuth? = null
    private lateinit var mBinding: ActivityProfileBinding
    private lateinit var mUser: User

    companion object {
        val TAG: String = ProfileActivity::class.java.simpleName
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityProfileBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        mAuth = FirebaseAuth.getInstance()
        getUser(); setSupportActionBar(mBinding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(true)
        mBinding.profile.tvProfileBirthDate.visibility = View.VISIBLE
        mBinding.social.tvTwitter.setOnTouchListener(this)
        mBinding.job.tvJob.setOnTouchListener(this)
        mBinding.achievement.btnReward.setOnClickListener {
            Intent(this, RewardActivity::class.java).let { intent ->
                intent.putExtra(Constant.USER, mUser)
                startActivity(intent)
            }
        }
    }

    private fun getUser() {
        HashMap<String, String>().let { header ->
            header["Accept"] = resources.getString(R.string.text_accept)
            header["Authorization"] =
                resources.getString(R.string.text_authorization, Companions.getAccessToken())
            Companions.makeJsonRequest(Request.Method.GET, Companions.user(), null, header,
                object : Callback {
                    override fun onSuccess(result: JSONObject?) {
                        mUser = Gson().fromJson(
                            result?.getJSONObject("user")?.toString(), User::class.java
                        )
                        val imageUri = (mUser.imageUri)?.replace("_normal", "")
                        val uri = if (mUser.person?.gender == "Male")
                            ContextCompat.getDrawable(
                                this@ProfileActivity, R.drawable.default_avatar_m
                            )
                        else
                            ContextCompat.getDrawable(
                                this@ProfileActivity, R.drawable.default_avatar_f
                            )

                        Glide.with(this@ProfileActivity).load(imageUri).placeholder(uri)
                            .into(mBinding.profile.ivProfileImage)
                        mBinding.profile.tvProfileName.text = mUser.person?.toString()
                        mBinding.profile.tvProfileAddress.text =
                            mUser.graduate?.contacts!![0].address
                        mBinding.profile.tvProfileBirthDate.text = mUser.person?.birthDate
                        if (mUser.graduate?.contacts!![0].contactNumber != null)
                            mBinding.profile.tvProfileContact.text =
                                mUser.graduate?.contacts!![0].contactNumber
                        if (mUser.graduate?.contacts!![0].email != null)
                            mBinding.profile.tvProfileEmail.text =
                                mUser.graduate?.contacts!![0].email

                        if (mUser.accounts!!.size > 0) {
                            for (i in mUser.accounts!!.indices) {
                                when (mUser.accounts!![i].providerName) {
                                    "linkedin.com" -> {
                                        mBinding.social.tvLinkedIn.text = resources.getString(
                                            R.string.text_id,
                                            mUser.accounts!![i].providerId
                                        )
                                    }
                                    "twitter.com" -> {
                                        mBinding.social.tvTwitter.text =
                                            resources.getString(
                                                R.string.text_tw_username,
                                                mUser.accounts!![i].username
                                            )
                                    }
                                }
                            }
                        }

                        mBinding.education.tvDegree.text = mUser.graduate?.academic?.course?.title
                        if (mUser.graduate?.academic?.course?.major.equals("None", true))
                            mBinding.education.tvMajor.visibility = View.GONE
                        else
                            mBinding.education.tvMajor.text = ("Major in ${mUser.graduate?.academic?.course?.major}")
                        mBinding.education.tvSchool.text = mUser.graduate?.academic?.school
                        mBinding.education.tvDateGraduated.text = resources?.getString(
                            R.string.text_date_graduated, mUser.graduate?.academic?.month,
                            mUser.graduate?.academic?.day, mUser.graduate?.academic?.year
                        )

                        if (mUser.graduate?.confirmedList!!.size > 0) {
                            val employment = mUser.graduate?.confirmedList!![0].employment
                            mBinding.job.tvCompanyName.text = employment?.company?.name
                            mBinding.job.tvCompanyAddress.text = employment?.company?.address
                            mBinding.job.tvPosition.text = employment?.jobPosition
                            mBinding.job.tvDateHired.text = employment?.dateEmployed
                        } else {
                            val employment = mUser.employments!![0]
                            mBinding.job.tvCompanyName.text = employment.company?.name
                            mBinding.job.tvCompanyAddress.text = employment.company?.address
                            mBinding.job.tvPosition.text = employment.jobPosition
                            mBinding.job.tvDateHired.text = employment.dateEmployed
                        }

                        mBinding.achievement.tvPoints.text =
                            resources.getString(R.string.text_points, mUser.achievement?.points)
                        mBinding.achievement.tvRank.text = mUser.achievement?.rank?.name
                    }

                    override fun onFailure(result: String?) {
                        Companions.debugLog(TAG, result)
                    }
                }
            )
        }
    }

    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
        v?.performClick()

        if (event?.action == MotionEvent.ACTION_DOWN) {
            when (v!!.id) {
                R.id.tvTwitter -> {
                    if (event.rawX >= v.right.minus((v as TextView).totalPaddingEnd)) {
                        val provider = OAuthProvider.newBuilder("twitter.com")
                        val pendingTaskResult = mAuth?.pendingAuthResult

                        if (pendingTaskResult != null) {
                            pendingTaskResult.addOnSuccessListener {
                                postSocialAccounts(it)
                            }.addOnFailureListener {
                                Companions.debugLog(TAG, it.message)
                            }
                        } else {
                            mAuth?.startActivityForSignInWithProvider(this, provider.build())
                                ?.addOnSuccessListener {
                                    postSocialAccounts(it)
                                }?.addOnFailureListener {
                                    Companions.debugLog(TAG, it.message)
                                }
                        }
                    } else {
                        v.requestFocus()
                    }
                }
                R.id.tvJob -> {
                    if (event.rawX >= v.right.minus((v as TextView).totalPaddingEnd)) {
                        Bundle().let { data ->
                            data.putParcelable(Constant.USER, mUser)
                            FormInputFragment().let { fragment ->
                                fragment.arguments = data
                                fragment.show(supportFragmentManager, TAG)
                            }
                        }
                    } else {
                        v.requestFocus()
                    }
                }
            }
        }

        return false
    }

    private fun postSocialAccounts(result: AuthResult) {
        val json = Gson().toJson(result.additionalUserInfo?.profile)
        val twitter = Gson().fromJson(json, Twitter::class.java)
        HashMap<String, String>().let { header ->
            header["Content-Type"] = resources.getString(R.string.text_accept)
            header["Authorization"] =
                resources.getString(R.string.text_authorization, Companions.getAccessToken())
            HashMap<String, String>().let { body ->
                body["providerId"] = twitter.id!!
                body["providerName"] = result.additionalUserInfo?.providerId!!
                body["username"] = twitter.username!!
                val maps: Array<Map<String, String>> = arrayOf(body, header)
                val url = Companions.socialAccount()
                Companions.makeJsonRequest(
                    Request.Method.POST, url, JSONObject(maps[0]), maps[1],
                    object : Callback {
                        override fun onSuccess(result: JSONObject?) {
                            Companions.toast(result?.getString("message"))
                            getUser()
                        }

                        override fun onFailure(result: String?) {
                            Companions.toast(result)
                        }
                    }
                )
            }
        }
    }

    override fun onDismiss(message: String?) {
        val dialog: AlertDialog

        getUser()

        AlertDialog.Builder(this).let { builder ->
            builder.setMessage(message).setCancelable(false)
                .setPositiveButton("OK") { dialog, _ ->
                    dialog.dismiss()
                }
            dialog = builder.create()
            dialog.show()
        }
    }

    override fun onResume() {
        super.onResume()
        getUser()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home)
            finish()

        return super.onOptionsItemSelected(item)
    }
}
