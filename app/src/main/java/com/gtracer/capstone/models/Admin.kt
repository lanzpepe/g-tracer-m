package com.gtracer.capstone.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class Admin(
    @SerializedName("admin_id") var adminId: String?, var username: String?,
    @SerializedName("user_id") var userId: String?, var pivot: AdminReward?,
    var departments: ArrayList<Department>?
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readParcelable(AdminReward::class.java.classLoader),
        parcel.createTypedArrayList(Department)
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(adminId)
        parcel.writeString(username)
        parcel.writeString(userId)
        parcel.writeParcelable(pivot, flags)
        parcel.writeTypedList(departments)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Admin> {
        override fun createFromParcel(parcel: Parcel): Admin {
            return Admin(parcel)
        }

        override fun newArray(size: Int): Array<Admin?> {
            return arrayOfNulls(size)
        }
    }
}