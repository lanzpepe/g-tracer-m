package com.gtracer.capstone.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.gtracer.capstone.R
import com.gtracer.capstone.databinding.FragmentImageBinding
import com.gtracer.capstone.utils.GlideApp

class ImageFragment : DialogFragment() {

    private var _binding: FragmentImageBinding? = null
    private val mBinding get() = _binding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentImageBinding.inflate(inflater, container, false)
        return mBinding?.root
    }

    override fun getTheme(): Int {
        return R.style.DialogTheme
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val items = arguments?.getStringArray("items")

        mBinding?.tvItemName?.text = items!![0]
        GlideApp.with(context!!).load(items[1]).into(mBinding?.ivItemPreview!!)

        mBinding?.btnDone?.setOnClickListener {
            dismiss()
        }
    }
}
