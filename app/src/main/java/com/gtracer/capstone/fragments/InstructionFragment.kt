package com.gtracer.capstone.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.gtracer.capstone.R
import com.gtracer.capstone.adapters.SliderAdapter
import com.gtracer.capstone.objects.Constant
import com.gtracer.capstone.objects.PrefsHelper
import com.google.android.material.tabs.TabLayout
import com.gtracer.capstone.databinding.FragmentInstructionBinding

class InstructionFragment : DialogFragment() {

    private var _binding: FragmentInstructionBinding? = null
    private val mBinding get() = _binding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentInstructionBinding.inflate(inflater, container, false)
        return mBinding?.root
    }

    override fun getTheme(): Int {
        return R.style.DialogTheme
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val gifs = arrayOf(
            R.drawable.swipe_left, R.drawable.swipe_right,
            R.drawable.submit_response, R.drawable.reward_items
        )

        isCancelable = false
        mBinding?.viewPager?.adapter = SliderAdapter(activity, gifs)
        mBinding?.tabLayout?.setupWithViewPager(mBinding?.viewPager, true)
        mBinding?.viewPager?.addOnPageChangeListener(
            TabLayout.TabLayoutOnPageChangeListener(
                mBinding?.tabLayout
            )
        )
        mBinding?.tabLayout?.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {}

            override fun onTabUnselected(tab: TabLayout.Tab?) {}

            override fun onTabSelected(tab: TabLayout.Tab?) {}
        })
        mBinding?.tabLayout?.bringToFront()
        mBinding?.btnDone?.setOnClickListener {
            PrefsHelper.writeBoolean(Constant.TUTORIAL, true)
            (activity as OnFragmentDialogListener).onDismiss()
            dismiss()
        }
    }

    interface OnFragmentDialogListener {
        fun onDismiss()
    }
}
