package com.gtracer.capstone.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class Education(
    @SerializedName("academic_id") var academicId: String? = null,
    var department: String? = null, var school: String? = null,
    var year: String? = null, var month: String? = null, var day: String? = null,
    var course: Course? = null
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readParcelable(Course::class.java.classLoader)
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(academicId)
        parcel.writeString(department)
        parcel.writeString(school)
        parcel.writeString(year)
        parcel.writeString(month)
        parcel.writeString(day)
        parcel.writeParcelable(course, flags)
    }

    override fun describeContents(): Int = 0

    companion object CREATOR : Parcelable.Creator<Education> {
        override fun createFromParcel(parcel: Parcel): Education = Education(parcel)

        override fun newArray(size: Int): Array<Education?> = arrayOfNulls(size)
    }
}